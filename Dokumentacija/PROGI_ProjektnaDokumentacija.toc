\babel@toc {croatian}{}\relax 
\contentsline {chapter}{\numberline {1}Dnevnik promjena dokumentacije}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Opis projektnog zadatka}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Cilj zadatka i potencijalna korist}{5}{section.2.1}%
\contentsline {section}{\numberline {2.2}Problematika zadatka}{5}{section.2.2}%
\contentsline {section}{\numberline {2.3}Korisnički zahtjevi}{6}{section.2.3}%
\contentsline {section}{\numberline {2.4}Mogućnost nadogradnje i prilagodbe rješenja}{7}{section.2.4}%
\contentsline {section}{\numberline {2.5}Novi aspekt problema i potencijalnih rješenja}{7}{section.2.5}%
\contentsline {section}{\numberline {2.6}Postojeća slična rješenja}{8}{section.2.6}%
\contentsline {chapter}{\numberline {3}Specifikacija programske potpore}{12}{chapter.3}%
\contentsline {section}{\numberline {3.1}Funkcionalni zahtjevi}{12}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Obrasci uporabe}{14}{subsection.3.1.1}%
\contentsline {subsubsection}{Opis obrazaca uporabe}{14}{subsubsection*.2}%
\contentsline {subsubsection}{Dijagrami obrazaca uporabe}{19}{subsubsection*.3}%
\contentsline {subsection}{\numberline {3.1.2}Sekvencijski dijagrami}{22}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}Ostali zahtjevi}{25}{section.3.2}%
\contentsline {chapter}{\numberline {4}Arhitektura i dizajn sustava}{26}{chapter.4}%
\contentsline {section}{\numberline {4.1}Baza podataka}{27}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Opis tablica}{28}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Dijagram baze podataka}{31}{subsection.4.1.2}%
\contentsline {section}{\numberline {4.2}Dijagram razreda}{32}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Dijagram razreda nakon završene aplikacije}{34}{subsection.4.2.1}%
\contentsline {section}{\numberline {4.3}Dijagram stanja}{41}{section.4.3}%
\contentsline {section}{\numberline {4.4}Dijagram aktivnosti}{42}{section.4.4}%
\contentsline {section}{\numberline {4.5}Dijagram komponenti}{44}{section.4.5}%
\contentsline {chapter}{\numberline {5}Implementacija i korisničko sučelje}{45}{chapter.5}%
\contentsline {section}{\numberline {5.1}Korištene tehnologije i alati}{45}{section.5.1}%
\contentsline {section}{\numberline {5.2}Ispitivanje programskog rješenja}{47}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Ispitivanje komponenti}{47}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Ispitivanje sustava}{51}{subsection.5.2.2}%
\contentsline {section}{\numberline {5.3}Dijagram razmještaja}{56}{section.5.3}%
\contentsline {section}{\numberline {5.4}Upute za puštanje u pogon}{57}{section.5.4}%
\contentsline {subsection}{\numberline {5.4.1}Dohvaćanje koda i izmjene u njemu}{57}{subsection.5.4.1}%
\contentsline {subsection}{\numberline {5.4.2}Pokretanje aplikacije}{60}{subsection.5.4.2}%
\contentsline {subsection}{\numberline {5.4.3}Pokretanje aplikacije na javnom poslužitelju}{61}{subsection.5.4.3}%
\contentsline {chapter}{\numberline {6}Zaključak i budući rad}{63}{chapter.6}%
\contentsline {chapter}{Popis literature}{65}{chapter*.4}%
\contentsline {chapter}{Indeks slika i dijagrama}{67}{chapter*.5}%
\contentsline {chapter}{Dodatak: Prikaz aktivnosti grupe}{68}{chapter*.6}%
