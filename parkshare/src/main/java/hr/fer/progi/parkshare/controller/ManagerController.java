package hr.fer.progi.parkshare.controller;


import hr.fer.progi.parkshare.model.ParkingLot;
import hr.fer.progi.parkshare.model.ParkingSpace;
import hr.fer.progi.parkshare.model.dto.*;
import hr.fer.progi.parkshare.repository.AppUserRepository;
import hr.fer.progi.parkshare.repository.ParkingLotRepository;
import hr.fer.progi.parkshare.servis.impl.AppUserServiceImpl;
import hr.fer.progi.parkshare.servis.impl.BookingInfoServiceImpl;
import hr.fer.progi.parkshare.servis.impl.ManagerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;


@CrossOrigin
@Controller
@RequestMapping("/api/manager")
public class ManagerController {

    @Autowired
    private ManagerServiceImpl managerService;

    @Autowired
    private AppUserServiceImpl userService;

    @Autowired
    private AppUserRepository appUserRepo;

    @Autowired
    private ParkingLotRepository parkingLotRepo;

    @Autowired
    private BookingInfoServiceImpl bookingInfoService;

    @GetMapping("/user/{username}")
    public ResponseEntity<UserViewDTO> userData(@PathVariable String username){
        return new ResponseEntity<UserViewDTO>(userService.userData(username), HttpStatus.OK);
    }

    @PostMapping("/create-parking-lot")
    public ResponseEntity<Boolean> createParkingLot(@RequestPart("maxBikes") String maxBikes, @RequestPart("name") String name, @RequestPart("description") String description,
                                                    @RequestPart("longitude1") String longitude1, @RequestPart("longitude2") String longitude2, @RequestPart("longitude3") String longitude3,
                                                    @RequestPart("longitude4") String longitude4, @RequestPart("latitude1") String latitude1, @RequestPart("latitude2") String latitude2,
                                                    @RequestPart("latitude3") String latitude3, @RequestPart("latitude4") String latitude4,
                                                    @RequestPart("username") String username, @RequestPart("file") MultipartFile file, @RequestPart("price1") String price1,  @RequestPart("price5") String price5,  @RequestPart("price1") String price10) {
        try {
            ParkingLot parkingLot = new ParkingLot(Integer.parseInt(maxBikes), Integer.parseInt(maxBikes), name, description, longitude1,
                                                    longitude2, longitude3, longitude4,
                                                    latitude1, latitude2, latitude3, latitude4,
                                                    appUserRepo.findByUsername(username), AppUserDTO.imageEncoder(file), Float.parseFloat(price1), Float.parseFloat(price5), Float.parseFloat(price10));
            return managerService.createParkingLot(parkingLot);
        } catch (Exception e) {
            return new ResponseEntity(Boolean.FALSE, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/create-parking-space")
    public ResponseEntity<Map<String, String>> createParkingSpace(@RequestPart("label") String label, @RequestPart("longitude1") String longitude1,
                                                  @RequestPart("longitude2") String longitude2, @RequestPart("longitude3") String longitude3,
                                                  @RequestPart("longitude4") String longitude4, @RequestPart("latitude1") String latitude1, @RequestPart("latitude2") String latitude2,
                                                  @RequestPart("latitude3") String latitude3, @RequestPart("latitude4") String latitude4, @RequestPart("parkingLotId") String parkingLotId) {
        try {
            ParkingSpace parkingSpace = new ParkingSpace(label, false, longitude1, longitude2, longitude3,
                                                                                    longitude4,latitude1, latitude2, latitude3,
                                                                                    latitude4, parkingLotRepo.findByParkingLotId(Integer.parseInt(parkingLotId)));
            return managerService.createParkingSpace(parkingSpace);
        } catch (Exception e) {
            return new ResponseEntity<Map<String, String>>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/show-parking-lots-manager/{username}")
    ResponseEntity<ParkingLotReturnDTO> showParkingLotsFromManager(@PathVariable String username) {
        try {
            return managerService.showParkingLotsFromManager(username);
        } catch (Exception e) {
            return new ResponseEntity<ParkingLotReturnDTO>(HttpStatus.NOT_FOUND);
        }
    }

        @PostMapping("/edit-parking-lot")
    ResponseEntity<String> editParkingLotInfo(@RequestPart("id") String id, @RequestPart("name") String name, @RequestPart("description") String description,
                                             @RequestPart("file") MultipartFile file, @RequestPart("price1") String price1, @RequestPart("price5") String price5, @RequestPart("price10") String price10) {
        try {
            ParkingLotEditDTO parkingLotEditDTO = new ParkingLotEditDTO(Integer.parseInt(id), name, description, file, Float.parseFloat(price1), Float.parseFloat(price5), Float.parseFloat(price10));
            return managerService.editParkingLotInfo(parkingLotEditDTO);
        } catch(Exception e) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("ps-reservable")
        ResponseEntity<String> defineReservability(@RequestPart("id") String id, @RequestPart("bookable") String bookable) {
        try {
            return managerService.defineReservability(Integer.parseInt(id), Boolean.parseBoolean(bookable));
        } catch(Exception e) {
            return new ResponseEntity("Parking space does not exist.", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("ps-delete")
    ResponseEntity<Boolean> deleteParkingSpace(@RequestPart("id") String id) {
        try {
            return managerService.deleteParkingSpace(Integer.parseInt(id));
        } catch (Exception e) {
            return new ResponseEntity(Boolean.FALSE, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/show-parking-spaces-manager/{parkingLotId}")
    ResponseEntity<List<ParkingSpaceDTO>> getParkingSpacesManager(@PathVariable String parkingLotId) {
        try {
            return managerService.getParkingSpacesManager(Integer.parseInt(parkingLotId));
        } catch (Exception e) {
            return new ResponseEntity<List<ParkingSpaceDTO>>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("ps-edit-coordinates")
    ResponseEntity<String> editParkingSpaceCoordinates(@RequestPart("id") String id,
                                                       @RequestPart("longitude1") String longitude1, @RequestPart("longitude2") String longitude2, @RequestPart("longitude3") String longitude3,
                                                       @RequestPart("longitude4") String longitude4, @RequestPart("latitude1") String latitude1, @RequestPart("latitude2") String latitude2,
                                                       @RequestPart("latitude3") String latitude3, @RequestPart("latitude4") String latitude4) {
        try {
            return managerService.editParkingSpaceCoordinates(Integer.valueOf(id), longitude1,  longitude2, longitude3, longitude4, latitude1, latitude2, latitude3, latitude4);
        } catch (Exception e) {
            //return new ResponseEntity("Parking space does not exist.", HttpStatus.NOT_FOUND);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Endpoint is used when a parking manager wants to
     * reserve or free a parking space for a customer that
     * didn't make a reservation over application.
     *
     * @param parkingSpaceId {@link Integer}
     * @param occupy boolean
     *
     * @return ResponseEntity with {@link String}
     * */
    @PostMapping("/manage-parking-space")
    public ResponseEntity<String> occupyParkingSpace(
            @RequestParam Integer parkingSpaceId, @RequestParam boolean occupy
            ) {
        System.out.println(occupy);
        if (occupy)
            return bookingInfoService.occupyParkingSpace(parkingSpaceId);
        else
            return bookingInfoService.freeParkingSpace(parkingSpaceId);
    }

    /**
     * Endpoint is used when a parking manager wants to
     * occupy or free a bike space.
     *
     * @param parkingLotId {@link Integer}
     * @param occupy boolean
     *
     * @return ResponseEntity with {@link String}
     * */
    @PostMapping("/manage-bike-space")
    public ResponseEntity<String> occupyBikeSpace(
            @RequestParam Integer parkingLotId, @RequestParam boolean occupy
    ) {
        if (occupy)
            return bookingInfoService.occupyBikeSpace(parkingLotId);
        else
            return bookingInfoService.freeBikeSpace(parkingLotId);
    }

    @PostMapping("/stats")
    public ResponseEntity<List<Integer>> displayStats(@RequestPart("username") String username) {
        try {
            TimeZone.setDefault(TimeZone.getTimeZone("GMT + 1"));
            LocalDate date = LocalDate.now();
            return managerService.getStatisticsLast7(username, date);
        } catch (Exception e) {
            return new ResponseEntity<List<Integer>>(HttpStatus.NOT_FOUND);
        }
    }
}
