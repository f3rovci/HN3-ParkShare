package hr.fer.progi.parkshare.servis;

import hr.fer.progi.parkshare.model.ParkingSpace;
import hr.fer.progi.parkshare.model.dto.BookingInfoDTO;
import hr.fer.progi.parkshare.model.dto.ReserveParkingSpaceDTO;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public interface BookingInfoService {

    /**
     * Method is used to get available appointments of given parking spaces.
     *
     * @param parkingSpaceIDs List of integers
     *
     * @return ResponseEntity<BookingInfoDTO>
     * */
    ResponseEntity<List<BookingInfoDTO>> getAvailableParkingSpaces(int parkingSpaceIDs);

    /**
     * Method is used to get available parking spaces for given startTime and endTime.
     *
     * @param startTime Timestamp
     * @param endTime List of integers
     *
     * @return ResponseEntity<BookingInfoDTO>
     * */
    ResponseEntity<List<ParkingSpace>> getAvailableAppointments(Timestamp startTime, Timestamp endTime);

    /**
     * Method takes a reservation for a {@link ParkingSpace} and makes
     * a record in {@link hr.fer.progi.parkshare.model.BookingInfo} table in
     * database. Returns if reservation was successful.
     *
     * @param reserveParkingSpaceDTO ReserveParkingSpaceDTO
     * @return ResponseEntity<String>
     * */
    ResponseEntity<String> reserveParkingSpace(ReserveParkingSpaceDTO reserveParkingSpaceDTO);

    /**
     * Method is used by parking manager to reserve a parking space when
     * someone comes to the parking lot without earlier reservation
     * and occupies a parking space.
     *
     * @param parkingSpaceId {@link Integer}
     *
     * @return ResponseEntity with {@link String}
     * */
    ResponseEntity<String> occupyParkingSpace(Integer parkingSpaceId);

    /**
     * Method is used by parking manager to free parking space occupied
     * by a costumer that didn't reserve his parking spot over the
     * application.
     *
     * @param parkingSpaceId {@link Integer}
     *
     * @return ResponseEntity with {@link String}
     * */
    ResponseEntity<String> freeParkingSpace(Integer parkingSpaceId);

    /**
     * Method is used by parking manager to reduce the number
     * of free bike parking spaces.
     *
     * @param  parkingLotId {@link Integer}
     *
     * @return ResponseEntity with {@link String}
     * */
    ResponseEntity<String> occupyBikeSpace(Integer parkingLotId);

    /**
     * Method is used by parking manager to free the occupied
     * bike parking spaces.
     *
     * @param parkingLotId {@link Integer}
     *
     * @return ResponseEntity with {@link String}
     * */
    ResponseEntity<String> freeBikeSpace(Integer parkingLotId);
}
