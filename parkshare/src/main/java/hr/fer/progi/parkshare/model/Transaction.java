package hr.fer.progi.parkshare.model;


import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Transaction {

    public Transaction(@NonNull Date date, float amount, @NonNull AppUser appUser) {
        this.date = date;
        this.amount = amount;
        this.appUser = appUser;
    }

    @Id
    @GeneratedValue
    @Column
    private int transactionId;

    @OneToOne(mappedBy = "transaction")
    private BookingInfo bookingInfo;

    @Column
    @NonNull
    private Date date;

    @Column
    private float amount;


    @NonNull
    @ManyToOne
    @JoinColumn(name = "username", nullable = false)
    private AppUser appUser;
}
