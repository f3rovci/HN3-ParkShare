package hr.fer.progi.parkshare.model.dto;

import hr.fer.progi.parkshare.model.allenum.Roles;
import liquibase.util.file.FilenameUtils;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppUserDTO {

    public AppUserDTO(@NonNull String username, @NonNull String password, @NonNull String firstName, @NonNull String lastName,  @NonNull String IBAN, @NonNull MultipartFile file, @NonNull String email, @NonNull String role) throws Exception {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.image = imageEncoder(file);
        this.IBAN = IBAN;

        if (role.toUpperCase().equals("CLIENT"))
            this.role = Roles.CLIENT;
        else if (role.toUpperCase().equals("PARKING_MANAGER"))
            this.role = Roles.PARKING_MANAGER;
        else throw new IllegalArgumentException();
    }

    public static String imageEncoder(MultipartFile file) {
        String encodedImage = new String();
        StringBuilder builder = new StringBuilder();
        try{
            String extension = FilenameUtils.getExtension(file.getName());
            byte[] bytes = file.getBytes();
            String encodeBase64 = Base64.getEncoder().encodeToString(bytes);
            encodedImage = builder.append("data:image/").append(extension).append(";base64,").append(encodeBase64).toString();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return encodedImage;
    }

    @NonNull
    private String username;

    @NonNull
    private String password;

    @NonNull
    private String firstName;


    @NonNull
    private String lastName;

    @NonNull
    private String IBAN;

    @NonNull
    private String image;

    @NonNull
    private String email;

    @NonNull
    private Roles role;
}
