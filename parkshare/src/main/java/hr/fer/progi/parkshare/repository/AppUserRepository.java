package hr.fer.progi.parkshare.repository;

import hr.fer.progi.parkshare.model.AppUser;
import hr.fer.progi.parkshare.model.allenum.Roles;
import hr.fer.progi.parkshare.model.dto.ManagerDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    AppUser findByEmail(String email);

    AppUser findByUsername(String username);

    List<AppUser> findByRole(Roles role);

}
