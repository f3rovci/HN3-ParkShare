package hr.fer.progi.parkshare.model;
import hr.fer.progi.parkshare.model.allenum.Roles;
import lombok.*;
import org.hibernate.validator.constraints.Length;


import javax.persistence.*;
import java.util.List;


@Entity
@Getter
@Setter
@NoArgsConstructor
public class AppUser {

    public AppUser(@NonNull String username, @NonNull String password, @NonNull String firstName, @NonNull String lastName, @NonNull String IBAN, @NonNull String image, @NonNull String email, @NonNull Roles role, float walletBalance) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.IBAN = IBAN;
        this.image = image;
        this.email = email;
        this.role = role;
        this.walletBalance = walletBalance;
    }

    @Column(name = "username")
    @Id
    @NonNull
    private String username;

    @OneToMany(mappedBy = "appUser")
    private List<BookingInfo> bookingInfoList;

    @OneToOne(mappedBy = "appUser")
    private ParkingLot parkingLot;

    @OneToMany(mappedBy = "appUser")
    private List<Transaction> transactionList;

    @Column
    @NonNull
    private String password;

    @Column
    @NonNull
    private String firstName;

    @Column
    @NonNull
    private String lastName;

    @Column
    @NonNull
    private String IBAN;

    @Length(max = 1000000)
    @Column
    @NonNull
    private String image;

    @Column
    @NonNull
    private String email;

    @Column
    @NonNull
    private Roles role;

    @Column
    private float walletBalance;
}
