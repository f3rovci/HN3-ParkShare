package hr.fer.progi.parkshare.model.dto;

import hr.fer.progi.parkshare.model.AppUser;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter

public class ParkingLotCreateDTO {

    public ParkingLotCreateDTO(int maxBikes, @NonNull String name, @NonNull String description, String longitude1, String longitude2, String longitude3, String longitude4, String latitude1, String latitude2, String latitude3, String latitude4, @NonNull AppUser appUser, @NonNull MultipartFile file, @NonNull Float price) {
        this.maxBikes = maxBikes;
        this.name = name;
        this.description = description;
        this.longitude1 = longitude1;
        this.longitude2 = longitude2;
        this.longitude3 = longitude3;
        this.longitude4 = longitude4;
        this.latitude1 = latitude1;
        this.latitude2 = latitude2;
        this.latitude3 = latitude3;
        this.latitude4 = latitude4;
        this.image = AppUserDTO.imageEncoder(file);
        this.price = price;
        this.appUser = appUser;
    }

    private int maxBikes;

    private int currentBikes = 0;

    @NonNull
    private String name;

    @NonNull
    private String description;

    private String longitude1;

    private String longitude2;

    private String longitude3;

    private String longitude4;

    private String latitude1;

    private String latitude2;

    private String latitude3;

    private String latitude4;

    @NonNull
    private AppUser appUser;

    @NonNull
    private String image;

    private float price;

}
