package hr.fer.progi.parkshare.model;


import lombok.*;
import org.hibernate.validator.constraints.Length;
import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ParkingLot {

    public ParkingLot(int maxBikes, int currentBikes, @NonNull String name, @NonNull String description, String longitude1, String longitude2, String longitude3, String longitude4, String latitude1, String latitude2, String latitude3, String latitude4, @NonNull AppUser appUser, @NonNull String image, float price1, float price5, float price10) {
        this.maxBikes = maxBikes;
        this.currentBikes = currentBikes;
        this.name = name;
        this.description = description;
        this.longitude1 = longitude1;
        this.longitude2 = longitude2;
        this.longitude3 = longitude3;
        this.longitude4 = longitude4;
        this.latitude1 = latitude1;
        this.latitude2 = latitude2;
        this.latitude3 = latitude3;
        this.latitude4 = latitude4;
        this.image = image;
        this.price1 = price1;
        this.price5 = price5;
        this.price10 = price10;
        this.appUser = appUser;
    }

    @Id
    @GeneratedValue
    @Column
    private int parkingLotId;

    @Column
    private int maxBikes;

    @Column
    private int currentBikes;

    @Column
    @NonNull
    private String name;

    @Column
    @NonNull
    private String description;

    @Column
    private String longitude1;

    @Column
    private String longitude2;

    @Column
    private String longitude3;

    @Column
    private String longitude4;

    @Column
    private String latitude1;

    @Column
    private String latitude2;

    @Column
    private String latitude3;

    @Column
    private String latitude4;

    @Length(max = 1000000)
    @NonNull
    @Column
    private String image;

    @Column
    private float price1;

    @Column
    private float price5;

    @Column
    private float price10;

    @NonNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "username", nullable = false)
    private AppUser appUser;

    @OneToMany(mappedBy = "parkingLot")
    private List<ParkingSpace> parkingSpaces;
}
