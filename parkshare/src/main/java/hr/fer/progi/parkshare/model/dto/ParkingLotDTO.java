package hr.fer.progi.parkshare.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ParkingLotDTO {
    private int parkingLotId;
    private String longitude1;
    private String longitude2;
    private String longitude3;
    private String longitude4;
    private String latitude1;
    private String latitude2;
    private String latitude3;
    private String latitude4;

}