package hr.fer.progi.parkshare.model.dto;

import hr.fer.progi.parkshare.model.BookingInfo;
import hr.fer.progi.parkshare.model.ParkingSpace;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class BookingInfoDTO {
    private Timestamp startTime;
    private Timestamp endTime;
    private boolean booked;
    private Integer parkingSpaceId;

    /**
     * Creates new {@link BookingInfoDTO} from {@link BookingInfo}.
     *
     * @param bookingInfo BookingInfo
     *
     * @return BookingInfoDto
     * */
    public static BookingInfoDTO modelToDTO(BookingInfo bookingInfo) {
        BookingInfoDTO bookingInfoDTO = new BookingInfoDTO();

        bookingInfoDTO.startTime = bookingInfo.getStartTime();
        bookingInfoDTO.endTime = bookingInfo.getEndTime();
        bookingInfoDTO.booked = bookingInfo.isBooked();
        bookingInfoDTO.parkingSpaceId = bookingInfo.getParkingSpace().getParkingSpaceId();

        return bookingInfoDTO;
    }
}
