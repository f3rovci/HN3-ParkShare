package hr.fer.progi.parkshare.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParkingSpace {

    public ParkingSpace(@NonNull String label, boolean booking, String longitude1, String longitude2, String longitude3, String longitude4, String latitude1, String latitude2, String latitude3, String latitude4, @NonNull ParkingLot parkingLot) {
        this.label = label;
        this.bookable = booking;
        this.longitude1 = longitude1;
        this.longitude2 = longitude2;
        this.longitude3 = longitude3;
        this.longitude4 = longitude4;
        this.latitude1 = latitude1;
        this.latitude2 = latitude2;
        this.latitude3 = latitude3;
        this.latitude4 = latitude4;
        this.parkingLot = parkingLot;
    }

    @Id
    @GeneratedValue
    @Column
    private Integer parkingSpaceId;

    @OneToMany(mappedBy = "parkingSpace")
    @JsonManagedReference
    private List<BookingInfo> bookingInfoList;

    @Column
    @NonNull
    private String label;

    @Column
    private boolean bookable;

    @Column
    private String longitude1;

    @Column
    private String longitude2;

    @Column
    private String longitude3;

    @Column
    private String longitude4;

    @Column
    private String latitude1;

    @Column
    private String latitude2;

    @Column
    private String latitude3;

    @Column
    private String latitude4;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "parkingLotId", nullable = false)
    private ParkingLot parkingLot;
}
