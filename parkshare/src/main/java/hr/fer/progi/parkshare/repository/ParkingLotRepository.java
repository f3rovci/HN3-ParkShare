package hr.fer.progi.parkshare.repository;

import hr.fer.progi.parkshare.model.ParkingLot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParkingLotRepository extends JpaRepository<ParkingLot, Integer> {
    ParkingLot findByParkingLotId(int id);
    ParkingLot findByAppUser_Username(String username);

}
