package hr.fer.progi.parkshare.model.dto;

import hr.fer.progi.parkshare.model.allenum.Roles;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserViewDTO {
    @NonNull
    private String username;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @NonNull
    private String IBAN;

    @NonNull
    private String image;

    @NonNull
    private String email;

    @NonNull
    private Roles role;

    private float walletBalance;
}
