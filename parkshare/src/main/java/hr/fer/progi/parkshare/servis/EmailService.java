package hr.fer.progi.parkshare.servis;

public interface EmailService {
    void send(String to, String email);
}
