package hr.fer.progi.parkshare.servis.impl;

import hr.fer.progi.parkshare.model.BookingInfo;
import hr.fer.progi.parkshare.model.ParkingLot;
import hr.fer.progi.parkshare.model.ParkingSpace;
import hr.fer.progi.parkshare.model.dto.*;
import hr.fer.progi.parkshare.repository.BookingInfoRepository;
import hr.fer.progi.parkshare.repository.ParkingLotRepository;
import hr.fer.progi.parkshare.repository.ParkingSpaceRepository;
import hr.fer.progi.parkshare.servis.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;

import java.util.concurrent.TimeUnit;

import java.util.stream.Collectors;


@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    ParkingLotRepository parkingLotRepo;

    @Autowired
    ParkingSpaceRepository parkingSpaceRepo;

    @Autowired
    BookingInfoRepository bookingInfoRepository;

    @Override
    public ResponseEntity<String> editParkingLotInfo(ParkingLotEditDTO parkingLotEditDTO) {
        String message = new String();
        HttpStatus status = HttpStatus.OK;
        ParkingLot parkingLot = parkingLotRepo.findByParkingLotId(parkingLotEditDTO.getIdPL());
        if (parkingLot != null) {
            parkingLot.setName(parkingLotEditDTO.getName());
            parkingLot.setDescription(parkingLotEditDTO.getDescription());
            parkingLot.setImage(parkingLotEditDTO.getImage());
            parkingLot.setPrice1(parkingLotEditDTO.getPrice1());
            parkingLot.setPrice5(parkingLotEditDTO.getPrice5());
            parkingLot.setPrice10(parkingLotEditDTO.getPrice10());
            parkingLotRepo.save(parkingLot);
            message = "Successfully added parking lot information.";
        } else {
            message = "Parking lot does not exist.";
        }
        return new ResponseEntity(message, status);
    }

    @Override
    public ResponseEntity<List<ParkingSpaceReturnDTO>> getEveryParkingSpace(int parkingLotId) {
        HttpStatus status = HttpStatus.OK;
        List<ParkingSpaceReturnDTO> listReturn = new ArrayList<>();
        ParkingLot parkingLot = parkingLotRepo.findByParkingLotId(parkingLotId);
        if (parkingLot != null) {
            List<ParkingSpace> parkingSpaces = parkingSpaceRepo.findByParkingLot_parkingLotId(parkingLotId);
            for (int i = 0; i < parkingSpaces.size(); ++i) {
                ParkingSpace parkingSpace = parkingSpaces.get(i);
                ParkingSpaceReturnDTO parkingSpaceReturnDTO = new ParkingSpaceReturnDTO(parkingSpace.getParkingSpaceId(), parkingSpace.getLabel(),
                        parkingSpace.isBookable(), parkingSpace.getLongitude1(), parkingSpace.getLongitude2(), parkingSpace.getLongitude3(),
                        parkingSpace.getLongitude4(), parkingSpace.getLatitude1(), parkingSpace.getLatitude2(), parkingSpace.getLatitude3(), parkingSpace.getLatitude4());
                listReturn.add(parkingSpaceReturnDTO);
            }
        } else {
          throw new IllegalArgumentException("Parking lot does not exist.");
        }
        return new ResponseEntity(listReturn, status);
    }

    @Override
    public ResponseEntity<List<ParkingSpaceReturnDTO>> getAvailableParkingSpace(int parkingLotId) {
        HttpStatus status = HttpStatus.OK;
        List<ParkingSpaceReturnDTO> availableParkingSpace = new ArrayList<>();
        ParkingLot parkingLot = parkingLotRepo.findByParkingLotId(parkingLotId);
        if (parkingLot != null) {
            List<ParkingSpace> parkingSpaces = parkingSpaceRepo.findByParkingLot_parkingLotId(parkingLotId);
            for (int i = 0; i < parkingSpaces.size(); ++i) {
                ParkingSpace parkingSpace = parkingSpaces.get(i);
                //if (parkingSpace.isBookable()) {
                    ParkingSpaceReturnDTO parkingSpaceReturnDTO = new ParkingSpaceReturnDTO(parkingSpace.getParkingSpaceId(), parkingSpace.getLabel(),
                            parkingSpace.isBookable(), parkingSpace.getLongitude1(), parkingSpace.getLongitude2(), parkingSpace.getLongitude3(),
                            parkingSpace.getLongitude4(), parkingSpace.getLatitude1(), parkingSpace.getLatitude2(), parkingSpace.getLatitude3(), parkingSpace.getLatitude4());
                    availableParkingSpace.add(parkingSpaceReturnDTO);
               // }
            }
        }
        return new ResponseEntity(availableParkingSpace, status);
    }

    @Override
    public ResponseEntity<String> defineReservability(int id, boolean bookable) {
        ParkingSpace parkingSpace = parkingSpaceRepo.findByParkingSpaceId(id);
        parkingSpace.setBookable(bookable);
        parkingSpaceRepo.save(parkingSpace);
        return new ResponseEntity("Successfully defined reservability.", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> createParkingLot(ParkingLot parkingLot) {
        parkingLotRepo.save(parkingLot);
        return new ResponseEntity(Boolean.TRUE, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Map<String, String>> createParkingSpace(ParkingSpace parkingSpace) {
        parkingSpaceRepo.save(parkingSpace);
        Map<String, String> mapa = new HashMap<>();
        mapa.put("boolean", "true");
        mapa.put("parkingSpaceId", ((Integer) parkingSpace.getParkingSpaceId()).toString());
        return new ResponseEntity(mapa, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ParkingLotReturnDTO> showParkingLotsFromManager(String username) {
        ParkingLot parkingLot = parkingLotRepo.findByAppUser_Username(username);
        ParkingLotReturnDTO parkingLotReturnDTO = new ParkingLotReturnDTO(parkingLot.getParkingLotId(), parkingLot.getMaxBikes(), parkingLot.getCurrentBikes(),
                                                                          parkingLot.getName(), parkingLot.getDescription(), parkingLot.getLongitude1(),
                                                                          parkingLot.getLongitude2(), parkingLot.getLongitude3(), parkingLot.getLongitude4(),
                                                                          parkingLot.getLatitude1(), parkingLot.getLatitude2(), parkingLot.getLatitude3(),
                                                                          parkingLot.getLatitude4(), parkingLot.getImage(), parkingLot.getPrice1(), parkingLot.getPrice5(), parkingLot.getPrice10());
        return new ResponseEntity(parkingLotReturnDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Boolean> deleteParkingSpace(int parkingSpaceId) {
        ParkingSpace parkingSpace = parkingSpaceRepo.findByParkingSpaceId(parkingSpaceId);
        parkingSpaceRepo.delete(parkingSpace);
        List<BookingInfo> bookingInfos = bookingInfoRepository.findByParkingSpace_ParkingSpaceId(parkingSpaceId);
        for (int i = 0; i < bookingInfos.size(); ++i) {
            bookingInfoRepository.delete(bookingInfos.get(i));
        }
        return new ResponseEntity(Boolean.TRUE, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ParkingSpaceDTO>> getParkingSpacesManager(int parkingLotId) {
        List<ParkingSpaceDTO> listReturn = new ArrayList<>();
        ParkingLot parkingLot = parkingLotRepo.findByParkingLotId(parkingLotId);
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        ts = new Timestamp(ts.getTime() + TimeUnit.HOURS.toMillis(1));

        if (parkingLot != null) {
            List<ParkingSpace> parkingSpaces = parkingSpaceRepo.findByParkingLot_parkingLotId(parkingLotId);
            for (int i = 0; i < parkingSpaces.size(); ++i) {
                ParkingSpace parkingSpace = parkingSpaces.get(i);

                List<BookingInfo> bookingInfos = bookingInfoRepository.findAllByParkingSpace(parkingSpace);
                bookingInfos = bookingInfos.stream().filter(b -> (b.getEndTime() == null)).collect(Collectors.toList());
                boolean occupiedByManager = false;
                if (!bookingInfos.isEmpty()) occupiedByManager = true;

                ParkingSpaceDTO parkingSpaceReturnDTO = new ParkingSpaceDTO(parkingSpace.getLongitude1(), parkingSpace.getLongitude2(), parkingSpace.getLongitude3(),
                        parkingSpace.getLongitude4(), parkingSpace.getLatitude1(), parkingSpace.getLatitude2(), parkingSpace.getLatitude3(), parkingSpace.getLatitude4(), parkingSpace.getParkingSpaceId(), parkingSpace.getLabel(),
                        parkingSpace.isBookable(), isAvailable(parkingSpace, ts), occupiedByManager) ;
                listReturn.add(parkingSpaceReturnDTO);
            }
        } else {
            throw new IllegalArgumentException("Parking lot does not exist.");
        }
        return new ResponseEntity(listReturn, HttpStatus.OK);
    }

    @Override
    public  ResponseEntity<String> editParkingSpaceCoordinates(int id, String longitude1, String longitude2, String longitude3, String longitude4, String latitude1, String latitude2, String latitude3, String latitude4) {
        ParkingSpace parkingSpace = parkingSpaceRepo.findByParkingSpaceId(id);
        parkingSpace.setLongitude1(longitude1);
        parkingSpace.setLongitude2(longitude2);
        parkingSpace.setLongitude3(longitude3);
        parkingSpace.setLongitude4(longitude4);
        parkingSpace.setLatitude1(latitude1);
        parkingSpace.setLatitude2(latitude2);
        parkingSpace.setLatitude3(latitude3);
        parkingSpace.setLatitude4(latitude4);
        parkingSpaceRepo.save(parkingSpace);
        return new ResponseEntity("Parking space coordinates successfuly changed.", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<Integer>> getStatisticsLast7(String username, LocalDate date) {
        int parkingLotId = parkingLotRepo.findByAppUser_Username(username).getParkingLotId();
        LocalDate dateLowerBound = date.minusDays(7);
        Timestamp timeLowerBound = Timestamp.valueOf(dateLowerBound.atStartOfDay());
        Timestamp timeUpperBound = Timestamp.valueOf(date.atStartOfDay());
        List<Integer> stats = new ArrayList<>();
        List<BookingInfo> bookingInfos = bookingInfoRepository.findAllByEndTimeBetween(timeLowerBound, timeUpperBound);
        for (int i = 7; i > 0; --i) {
            int reservationCounter = 0;
            LocalDate searchDate = date.minusDays(i);
            for (int j = 0; j < bookingInfos.size(); ++j) {
                BookingInfo bookingInfo = bookingInfos.get(j);
                LocalDate infoDate = bookingInfo.getEndTime().toLocalDateTime().toLocalDate();
                if (infoDate.equals(searchDate)) {
                    if (parkingLotId == bookingInfo.getParkingSpace().getParkingLot().getParkingLotId()) {
                        ++reservationCounter;
                    }
                }
            }
            stats.add(reservationCounter);
        }
        return new ResponseEntity(stats, HttpStatus.OK);
    }

    private boolean isAvailable(ParkingSpace parkingSpace, Timestamp timestamp){
        List<BookingInfo> list = bookingInfoRepository.findByParkingSpace_ParkingSpaceId(parkingSpace.getParkingSpaceId());
        if (list.isEmpty())
            return true;
        for (BookingInfo bookingInfo: list){
            if (bookingInfo.getEndTime() == null)
                return false;
            else if(timestamp.compareTo(bookingInfo.getStartTime()) >=0 && timestamp.compareTo(bookingInfo.getEndTime()) < 0){
                return false;
            }
        }
        return true;
    }
}
