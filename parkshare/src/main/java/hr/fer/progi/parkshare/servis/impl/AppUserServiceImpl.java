package hr.fer.progi.parkshare.servis.impl;

import hr.fer.progi.parkshare.model.*;

import hr.fer.progi.parkshare.model.allenum.Roles;

import hr.fer.progi.parkshare.model.dto.*;

import hr.fer.progi.parkshare.repository.AppUserRepository;
import hr.fer.progi.parkshare.repository.BookingInfoRepository;
import hr.fer.progi.parkshare.repository.ParkingLotRepository;
import hr.fer.progi.parkshare.repository.ParkingSpaceRepository;
import hr.fer.progi.parkshare.servis.AppUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.validator.routines.IBANValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
@RequiredArgsConstructor
public class AppUserServiceImpl implements AppUserService, UserDetailsService{
    @Autowired
    BookingInfoRepository bookingInfoRepository;

    @Autowired
    ParkingLotRepository parkingLotRepository;

    @Autowired
    ParkingSpaceRepository parkingSpaceRepository;

    @Autowired
    AppUserRepository userRepository;

    @Autowired
    EmailServisImpl emailServis;

    @Autowired
    PasswordEncoder passwordEncoder;

    private IBANValidator validator = new IBANValidator();

    private final VerificationTokenServiceImpl verificationTokenService;

    @Value("${email.verify.account.link}")
    private String verifyAccountUrl;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser user = userRepository.findByUsername(username);
        if (user == null) {
            log.error("User not found in the database");
            throw  new UsernameNotFoundException("User not found in the database");
        } else{
            log.info("User found in the database: {}", username);
        }

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority((user.getRole()).toString()));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
    }



    @Override
    public AppUser createUser(AppUser appUser) {
        AppUser appUserExists = userRepository.findByEmail(appUser.getEmail());
        if (appUserExists != null)
            throw new IllegalArgumentException("User already exists");

        appUser.setPassword(passwordEncoder.encode(appUser.getPassword()));
        userRepository.save(appUser);

        return appUser;
    }

    @Override
    public AppUser getUser(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public ResponseEntity<Map<String, String>> register(AppUserDTO appUser) {

        var responseStatus = HttpStatus.OK;
        String message = new String();

        appUser.setPassword(passwordEncoder.encode(appUser.getPassword()));
        AppUser existingAppUserEmail = userRepository.findByEmail(appUser.getEmail());
        AppUser existingAppUserUsername = userRepository.findByUsername(appUser.getUsername());
        Map<String, String> ret = new HashMap<>();
        if (existingAppUserEmail != null) {
            ret.put("boolean", "false");
            message = "A user is already registered with that email.";
            if (existingAppUserUsername != null) {
                message += " and the username is taken.";
            } else message += ".";
        } else if (existingAppUserUsername != null) {
            message = "The username is taken.";
        } else if(!validator.isValid(appUser.getIBAN())){
            message = "IBAN is not valid.";
        }
        else {
            ret.put("boolean", "true");
            Roles userRole = appUser.getRole();
            if (appUser.getRole().equals(Roles.CLIENT)) {
                appUser.setRole(Roles.PENDING);
            } else if (appUser.getRole().equals(Roles.PARKING_MANAGER)) {
                appUser.setRole(Roles.UNAPPROVED_MAIL);
            }
            message = "Welcome to ParkShare. Verify your account via email.";
            AppUser user = new AppUser(appUser.getUsername(), appUser.getPassword(), appUser.getFirstName(), appUser.getLastName(), appUser.getIBAN(), appUser.getImage(), appUser.getEmail(), appUser.getRole(), 0);
            userRepository.save(user);
            user.setRole(userRole);
            VerificationToken token = verificationTokenService.createToken(user);
            emailServis.send(
                    appUser.getEmail(),
                    emailServis.buildEmail(
                            appUser.getUsername(),
                            verifyAccountUrl+token.getToken()
                    )
            );
        }
        ret.put("message", message);

        return new ResponseEntity<Map<String, String>>(ret, responseStatus);
    }

    public ResponseEntity<String> verifyToken(String token) {
        VerificationToken verificationToken = verificationTokenService.getToken(token);

        if (verificationToken == null)
            throw new IllegalStateException("Token not found.");

        if (verificationToken.getVerifiedAt() != null)
            throw new IllegalStateException("Email already verified.");

        if (verificationToken.getExpiresAt().isBefore(LocalDateTime.now()))
            throw new IllegalStateException("Token expired.");

        verificationTokenService.setVerifiedAt(token);
        enableAppUser(verificationToken.getAppUser().getEmail());

        AppUser appUser = userRepository.findByEmail(verificationToken.getAppUser().getEmail());
        System.out.println(appUser.getRole());

        return new ResponseEntity<String>("User successfully verified!", HttpStatus.OK);
    }

    private void enableAppUser(String email) {
        AppUser appUser = userRepository.findByEmail(email);

        if (appUser.getRole() == Roles.PENDING)
            appUser.setRole(Roles.CLIENT);
        else
            appUser.setRole(Roles.UNAPPROVED);

        userRepository.save(appUser);
    }

    @Override
    public List<AppUser> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public UserViewDTO userData(String username) {
        AppUser user = userRepository.findByUsername(username);
        return new UserViewDTO(user.getUsername(), user.getFirstName(), user.getLastName(), user.getIBAN(), user.getImage(), user.getEmail(),user.getRole(), user.getWalletBalance());
    }

    @Override
    public CarParkingSpaceDTO findClosestCar(float longitude, float latitude, Timestamp timestamp) {
        var list = parkingSpaceRepository.findAll();
        CarParkingSpaceDTO ret = null;
        float min = 0;
        int i = 0;
        for (ParkingSpace ps: list){
            float x, y;
            x = (Float.valueOf(ps.getLongitude1()) + Float.valueOf(ps.getLongitude3())) / 2;
            y = (Float.valueOf(ps.getLatitude1()) + Float.valueOf(ps.getLatitude3())) / 2;

            System.out.println(ps.getParkingSpaceId());
            System.out.println(getDistance(x, y, latitude, longitude));
            System.out.println(isAvailable(ps, timestamp));
            if (isAvailable(ps, timestamp)) {
                if (i == 0 || getDistance(x, y, latitude, longitude) < min) {
                    System.out.println("isus");
                    ret = new CarParkingSpaceDTO(ps.getLongitude1(), ps.getLongitude2(), ps.getLongitude3(), ps.getLongitude4(), ps.getLatitude1(), ps.getLatitude2(), ps.getLatitude3(), ps.getLatitude4(), ps.getParkingSpaceId(), ps.isBookable());
                    min = getDistance(x, y, latitude, longitude);
                    i++;
                }
            }
        }

        return ret;
    }


    private float getDistance(float x1, float y1, float x2, float y2){
        float distancex, distancey;
        distancex = x2 - x1;
        distancey = y2 - y1;
        return (float) Math.sqrt(Math.pow(distancex, 2) + Math.pow(distancey, 2));
    }

    private boolean isAvailable(ParkingSpace parkingSpace, Timestamp timestamp){
        List<BookingInfo> list = bookingInfoRepository.findByParkingSpace_ParkingSpaceId(parkingSpace.getParkingSpaceId());
        if (list.isEmpty())
            return true;
        for (BookingInfo bookingInfo: list){
            if (bookingInfo.getEndTime() == null)
                return false;
            else if(timestamp.compareTo(bookingInfo.getStartTime()) >=0 && timestamp.compareTo(bookingInfo.getEndTime()) < 0){
                return false;
            }
        }
        return true;
    }

    @Override
    public Map<String, String> findClosestBike(float longitude, float latitude) {
        var list = parkingLotRepository.findAll();
        Map<String, String> response = new HashMap<>();
        if (list.size() == 0){
            response.put("found", "false");
            response.put("error", "There is currently no parkings for bikes");
            return response;
        }
        float distancex;
        float distancey;
        double distance = 0;
        boolean bool = false;
        int index = 0;
        float longitudeCenter = 0;
        float latitudeCenter = 0;
        for (int i = 0; i < list.size(); i++){
            ParkingLot pl = list.get(i);
            distancex = (Float.valueOf(pl.getLongitude1()) + Float.valueOf(pl.getLongitude2()) + Float.valueOf(pl.getLongitude3()) + Float.valueOf(pl.getLongitude4())) / 4 - latitude;
            distancey = (Float.valueOf(pl.getLatitude1()) + Float.valueOf(pl.getLatitude2()) + Float.valueOf(pl.getLatitude3()) + Float.valueOf(pl.getLatitude4())) / 4 - longitude;
            if (!bool && (pl.getCurrentBikes() > 0)){
                distance = Math.sqrt(Math.pow(distancex, 2) + Math.pow(distancey, 2));
                bool = true;
                index = i;
                longitudeCenter = (Float.valueOf(pl.getLongitude1()) + Float.valueOf(pl.getLongitude2()) + Float.valueOf(pl.getLongitude3()) + Float.valueOf(pl.getLongitude4())) / 4;
                latitudeCenter = (Float.valueOf(pl.getLatitude1()) + Float.valueOf(pl.getLatitude2()) + Float.valueOf(pl.getLatitude3()) + Float.valueOf(pl.getLatitude4())) / 4;
            } else if (bool && (pl.getCurrentBikes() > 0)){
                if ((Math.sqrt(Math.pow(distancex, 2) + Math.pow(distancey, 2))) < distance) {
                    distance = Math.sqrt(Math.pow(distancex, 2) + Math.pow(distancey, 2));
                    index = i;
                    longitudeCenter = (Float.valueOf(pl.getLongitude1()) + Float.valueOf(pl.getLongitude2()) + Float.valueOf(pl.getLongitude3()) + Float.valueOf(pl.getLongitude4())) / 4;
                    latitudeCenter = (Float.valueOf(pl.getLatitude1()) + Float.valueOf(pl.getLatitude2()) + Float.valueOf(pl.getLatitude3()) + Float.valueOf(pl.getLatitude4())) / 4;
                }
            }
        }
        if (!bool){
            response.put("found", "false");
            response.put("error", "There is currently no parking spaces available for bikes");
            return response;
        }

        response.put("found", "true");
        response.put("longitude", Float.valueOf(longitudeCenter).toString());
        response.put("latitude", Float.valueOf(latitudeCenter).toString());
        return response;
    }

    @Override
    public ResponseEntity<List<ParkingLotReturnDTO>> clientViewParkingLots() {
        List<ParkingLotReturnDTO> returnList = new ArrayList<>();
        List<ParkingLot> listParkingLots = parkingLotRepository.findAll();
        for (int i = 0; i < listParkingLots.size(); ++i) {
            ParkingLot parkingLot = listParkingLots.get(i);
            ParkingLotReturnDTO parkingLotReturnDTO = new ParkingLotReturnDTO(parkingLot.getParkingLotId(), parkingLot.getMaxBikes(), parkingLot.getCurrentBikes(),
                                                                              parkingLot.getName(), parkingLot.getDescription(), parkingLot.getLongitude1(),
                                                                              parkingLot.getLongitude2(), parkingLot.getLongitude3(), parkingLot.getLongitude4(),
                                                                              parkingLot.getLatitude1(), parkingLot.getLatitude2(), parkingLot.getLatitude3(),
                                                                              parkingLot.getLatitude4(), parkingLot.getImage(), parkingLot.getPrice1(), parkingLot.getPrice5(), parkingLot.getPrice10());
            returnList.add(parkingLotReturnDTO);
        }
        return new ResponseEntity(returnList, HttpStatus.OK);
    }

    @Override
    public List<ParkingLotDTO> getAllParkingLotsDTO() {

        var list = parkingLotRepository.findAll();
        List<ParkingLotDTO> ret = new ArrayList<>();

        for (int i = 0; i < list.size(); i++){
            ParkingLot pl = list.get(i);
            ret.add(new ParkingLotDTO(pl.getParkingLotId(),pl.getLongitude1(),pl.getLongitude2(),pl.getLongitude3(),pl.getLongitude4(),pl.getLatitude1(),pl.getLatitude2(),pl.getLatitude3(),pl.getLatitude4()));
        }

        return ret;
    }

    @Override
    public List<CarParkingSpaceDTO> getParkingSpacesDTO() {
        var list = parkingSpaceRepository.findAll();
        List<CarParkingSpaceDTO> ret = new ArrayList<>();

        for (int i = 0; i < list.size(); i++){

            ParkingSpace ps = list.get(i);

            ret.add(new CarParkingSpaceDTO(ps.getLongitude1(), ps.getLongitude2(), ps.getLongitude3(), ps.getLongitude4(),ps.getLatitude1(), ps.getLatitude2(), ps.getLatitude3(), ps.getLatitude4(),ps.getParkingSpaceId(),ps.isBookable()));
        }
        return ret;
    }

    @Override
    public List<ParkingSpaceDTO> getAvailableParkingSpacesDTO(Timestamp timestamp) {

        var list = parkingSpaceRepository.findAll();
        List<ParkingSpaceDTO> ret = new ArrayList<>();

        for (int i = 0; i < list.size(); i++){

            ParkingSpace ps = list.get(i);
            List<BookingInfo> bookingInfos = bookingInfoRepository.findAllByParkingSpace(ps);
            bookingInfos = bookingInfos.stream().filter(b -> (b.getEndTime() == null)).collect(Collectors.toList());
            boolean occupiedByManager = false;
            if (!bookingInfos.isEmpty()) occupiedByManager = true;
            ret.add(new ParkingSpaceDTO(ps.getLongitude1(), ps.getLongitude2(), ps.getLongitude3(), ps.getLongitude4(),ps.getLatitude1(), ps.getLatitude2(), ps.getLatitude3(), ps.getLatitude4(),ps.getParkingSpaceId(), ps.getLabel(), ps.isBookable(),isAvailable(ps,timestamp), occupiedByManager));
        }

        return ret;
    }

    @Override
    public Map<String, String> updateWallet(float famount, String username) {
        AppUser user = userRepository.findByUsername(username);
        Map<String, String> ret = new HashMap<>();
        if (user == null) {
            String s = "User with username " + username + " does not exist!";
            ret.put("message", s);
            ret.put("successful", "false");
            return ret;
        }
        if (famount < 0){
            String s = "You entered wrong amount";
            ret.put("message", s);
            ret.put("successful", "false");
            return ret;
        }

        user.setWalletBalance(user.getWalletBalance() + famount);
        String s = "You successfully added " + famount + " to your wallet";
        ret.put("message", s);
        ret.put("successful", "true");
        userRepository.save(user);
        return ret;
    }

}

