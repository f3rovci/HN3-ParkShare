package hr.fer.progi.parkshare.controller;

import hr.fer.progi.parkshare.model.allenum.Roles;
import hr.fer.progi.parkshare.model.dto.AdminViewUserDTO;
import hr.fer.progi.parkshare.model.dto.ManagerDTO;
import hr.fer.progi.parkshare.repository.AppUserRepository;
import hr.fer.progi.parkshare.servis.impl.AdminServiceImpl;
import hr.fer.progi.parkshare.servis.impl.AppUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@Controller
@RequestMapping("/api/admin")
public class AdminController {
    @Autowired
    private AppUserRepository userRepository;

    @Autowired
    private AdminServiceImpl adminService;

    @Autowired
    private AppUserServiceImpl userService;

    @GetMapping("/unapprovedmanagers")
    public ResponseEntity<List<ManagerDTO>> getManagers(){

        return new ResponseEntity<List<ManagerDTO>>(userRepository.findByRole(Roles.UNAPPROVED).stream().map(u -> new ManagerDTO(u.getFirstName(), u.getLastName(), u.getUsername())).collect(Collectors.toList()), HttpStatus.OK);
    }

    @PostMapping("/unapprovedmanagers/{username}")
    public ResponseEntity<String> promoteToManager(@PathVariable String username){
        return adminService.promoteManager(username);
    }

    @GetMapping("/users")
    public ResponseEntity<List<AdminViewUserDTO>> getRegisteredUsers(){
        return new ResponseEntity<List<AdminViewUserDTO>>(adminService.getRegisteredUsers(), HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<String> editAppUserInfo(@Valid @RequestPart("username") String username,
                                                  @RequestPart("firstName") String firstName, @RequestPart("lastName") String lastName,
                                                  @RequestPart("iban") String iban, @RequestPart("email") String email, @RequestPart("image") MultipartFile image, @RequestPart("role") String role) throws Exception {
        try {
            AdminViewUserDTO appUser = new AdminViewUserDTO(username, firstName, lastName, iban, image, email, role);
            return adminService.editAppUserInfo(appUser);
        }catch (Exception e){
            return  new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/users/{username}")
    public ResponseEntity<String> promoteToAdmin(@PathVariable String username){
        return adminService.promoteAdmin(username);
    }

}
