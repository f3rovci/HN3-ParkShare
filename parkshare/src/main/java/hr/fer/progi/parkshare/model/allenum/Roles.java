package hr.fer.progi.parkshare.model.allenum;

public enum Roles {
    PENDING,
    UNAPPROVED,
    UNAPPROVED_MAIL,
    CLIENT,
    PARKING_MANAGER,
    ADMIN
}
