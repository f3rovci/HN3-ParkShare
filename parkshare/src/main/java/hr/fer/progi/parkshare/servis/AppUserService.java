package hr.fer.progi.parkshare.servis;

import hr.fer.progi.parkshare.model.AppUser;
import hr.fer.progi.parkshare.model.dto.*;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public interface AppUserService {
    AppUser createUser(AppUser appUser);

    AppUser getUser(String email);

    ResponseEntity<Map<String,String>> register(AppUserDTO appUser);

    List<AppUser> getUsers();

    UserViewDTO userData(String username);

    CarParkingSpaceDTO findClosestCar(float longitude, float latitude, Timestamp timestamp);

    Map<String,String> findClosestBike(float longitude, float latitude);

    ResponseEntity<List<ParkingLotReturnDTO>> clientViewParkingLots();

    List<ParkingLotDTO> getAllParkingLotsDTO();

    List<CarParkingSpaceDTO> getParkingSpacesDTO();

    List<ParkingSpaceDTO> getAvailableParkingSpacesDTO(Timestamp timestamp);

    Map<String, String> updateWallet(float famount, String username);
}
