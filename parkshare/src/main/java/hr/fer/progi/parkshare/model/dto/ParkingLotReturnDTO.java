package hr.fer.progi.parkshare.model.dto;

import hr.fer.progi.parkshare.model.AppUser;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class ParkingLotReturnDTO {
    public ParkingLotReturnDTO(int id, int maxBikes, int currentBikes, String name, String description, String longitude1, String longitude2, String longitude3, String longitude4, String latitude1, String latitude2, String latitude3, String latitude4, String image, float price1, float price5, float price10) {
        this.id = id;
        this.maxBikes = maxBikes;
        this.currentBikes = currentBikes;
        this.name = name;
        this.description = description;
        this.longitude1 = longitude1;
        this.longitude2 = longitude2;
        this.longitude3 = longitude3;
        this.longitude4 = longitude4;
        this.latitude1 = latitude1;
        this.latitude2 = latitude2;
        this.latitude3 = latitude3;
        this.latitude4 = latitude4;
        this.image = image;
        this.price1 = price1;
        this.price5 = price5;
        this.price10 = price10;
    }

    private int id;
    private int maxBikes;
    private int currentBikes;
    private String name;
    private String description;
    private String longitude1;
    private String longitude2;
    private String longitude3;
    private String longitude4;
    private String latitude1;
    private String latitude2;
    private String latitude3;
    private String latitude4;
    private String image;
    private float price1;
    private float price5;
    private float price10;

}
