package hr.fer.progi.parkshare.model.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class ParkingLotEditDTO {

    public ParkingLotEditDTO(int id, @NonNull String name, @NonNull String description, @NonNull MultipartFile file, float price1,float price5, float price10) {
        this.idPL = id;
        this.name = name;
        this.description = description;
        this.image = AppUserDTO.imageEncoder(file);
        this.price1 = price1;
        this.price5 = price5;
        this.price10 = price10;
    }

    private int idPL;

    @NonNull
    private String name;

    @NonNull
    private String description;

    @NonNull
    private String image;

    private float price1;
    private float price5;
    private float price10;
}
