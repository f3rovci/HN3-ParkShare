package hr.fer.progi.parkshare.controller;

import hr.fer.progi.parkshare.model.ParkingSpace;
import hr.fer.progi.parkshare.model.dto.BookingInfoDTO;
import hr.fer.progi.parkshare.model.dto.ReserveParkingSpaceDTO;
import hr.fer.progi.parkshare.servis.BookingInfoService;
import hr.fer.progi.parkshare.servis.impl.BookingInfoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.TimeUnit;


@Controller
@RequestMapping("/api/parking")
public class ParkingController {
    @Autowired
    private BookingInfoServiceImpl bookingInfoService;

    /**
     * Method is used to get available appointments of given parking spaces.
     *
     * @param id String
     *
     * @return ResponseEntity<BookingInfoDTO>
     * */
    @PostMapping("/availableParkingSpaces")
    public ResponseEntity<List<BookingInfoDTO>> getAvailableParkingSpaces(
            @RequestPart("id") String id) {

        return bookingInfoService.getAvailableParkingSpaces(Integer.parseInt(id));
    }

    /**
     * Method is used to get available parking spaces for given startTime and endTime.
     *
     * @param startTime Timestamp
     * @param endTime List of integers
     *
     * @return ResponseEntity<BookingInfoDTO>
     * */
    @GetMapping("/availableAppointments")
    public ResponseEntity<List<ParkingSpace>> getAvailableAppointments(
            @RequestParam("startTime")Timestamp startTime,
            @RequestParam("endTime")Timestamp endTime
    ) {

        return bookingInfoService.getAvailableAppointments(startTime, endTime);
    }

    /**
     * Method takes a reservation for a {@link ParkingSpace} and makes
     * a record in {@link hr.fer.progi.parkshare.model.BookingInfo} table in
     * database.
     *
     * @param reserveParkingSpaceDTO ReserveParkingSpaceDTO
     * @return ResponseEntity<String>
     * */
    @PostMapping("/reservation")
    public ResponseEntity<String> reserveParkingSpace(
            @RequestBody ReserveParkingSpaceDTO reserveParkingSpaceDTO
    ) {

        return bookingInfoService.reserveParkingSpace(reserveParkingSpaceDTO);
    }

    @PostMapping("/reservationDefault")
    public ResponseEntity<String> reserveParkingSpaceDefault(
            @RequestPart ("username") String username, @RequestPart ("id") String id
    ) {
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        ts = new Timestamp(ts.getTime() + TimeUnit.HOURS.toMillis(1));
        Instant now = Instant.now();
        Duration diff = Duration.between(
                LocalTime.MIN,
                LocalTime.parse("02:00:00")
        );
        Instant res = now.plus(diff);
        ReserveParkingSpaceDTO reserveParkingSpaceDTO = new ReserveParkingSpaceDTO(ts, Timestamp.from(res), Integer.parseInt(id), username, false, false);
        return bookingInfoService.reserveParkingSpace(reserveParkingSpaceDTO);
    }
}
