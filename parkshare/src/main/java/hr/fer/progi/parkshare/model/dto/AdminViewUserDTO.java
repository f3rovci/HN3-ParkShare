package hr.fer.progi.parkshare.model.dto;

import hr.fer.progi.parkshare.model.allenum.Roles;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AdminViewUserDTO {

    public AdminViewUserDTO(@NonNull String username, @NonNull String firstName, @NonNull String lastName, @NonNull String IBAN, @NonNull MultipartFile file, @NonNull String email, @NonNull String role) throws Exception {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.IBAN = IBAN;
        this.image = AppUserDTO.imageEncoder(file);
        this.email = email;

        if (role.toUpperCase().equals("CLIENT"))
            this.role = Roles.CLIENT;
        else if (role.toUpperCase().equals("PARKING_MANAGER"))
            this.role = Roles.PARKING_MANAGER;
        else throw new IllegalArgumentException();
    }

    @NonNull
    private String username;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @NonNull
    private String IBAN;

    @NonNull
    private String image;

    @NonNull
    private String email;

    @NonNull
    private Roles role;
}
