package hr.fer.progi.parkshare.servis.impl;

import hr.fer.progi.parkshare.model.AppUser;
import hr.fer.progi.parkshare.model.VerificationToken;
import hr.fer.progi.parkshare.repository.VerificationTokenRepository;
import hr.fer.progi.parkshare.servis.VerificationTokenServis;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@AllArgsConstructor
public class VerificationTokenServiceImpl implements VerificationTokenServis {

    private final VerificationTokenRepository verificationTokenRepository;

    public void saveVerificationToken(VerificationToken token) {
        verificationTokenRepository.save(token);
    }

    public VerificationToken getToken(String token) {
       return verificationTokenRepository.findByToken(token).get();
    }

    public void setVerifiedAt(String token) {
        VerificationToken verificationToken = verificationTokenRepository.findByToken(token).get();

        verificationToken.setVerifiedAt(LocalDateTime.now());

        verificationTokenRepository.save(verificationToken);
    }

    public VerificationToken createToken(AppUser appUser) {
        String token = UUID.randomUUID().toString();

        VerificationToken verificationToken = new VerificationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(60),
                appUser
        );

        saveVerificationToken(verificationToken);

        return verificationToken;
    }
}
