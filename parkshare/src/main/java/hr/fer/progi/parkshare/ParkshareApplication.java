package hr.fer.progi.parkshare;

import hr.fer.progi.parkshare.model.AppUser;
import hr.fer.progi.parkshare.model.allenum.Roles;
import hr.fer.progi.parkshare.servis.AppUserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableWebSecurity
public class ParkshareApplication {

	public static void main(String[] args) {
		System.setProperty("user.timezone", "Europe/Croatia");
		SpringApplication.run(ParkshareApplication.class, args);
	}

/*
	@Bean
	CommandLineRunner run(AppUserService userService){
		return args -> {
			userService.createUser(new AppUser("username1", "password", "john", "wick", "iban", "image", "email1", Roles.PARKING_MANAGER, 0));

			userService.createUser(new AppUser("Hurrinade", "qwerty", "john", "wick", "iban", "image", "email2", Roles.CLIENT, 0));

			userService.createUser(new AppUser("username", "password", "john", "wick", "iban", "image", "email", Roles.ADMIN, 0));
		};
	}
*/

	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("https://f3rovci-parkshare-frontend.herokuapp.com/");
			}
		};
	}

}
