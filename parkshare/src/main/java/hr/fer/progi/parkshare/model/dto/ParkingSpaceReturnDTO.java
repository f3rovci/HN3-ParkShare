package hr.fer.progi.parkshare.model.dto;

import hr.fer.progi.parkshare.model.BookingInfo;
import hr.fer.progi.parkshare.model.ParkingLot;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
public class ParkingSpaceReturnDTO {
    public ParkingSpaceReturnDTO(int id, String label, boolean booking, String longitude1, String longitude2, String longitude3, String longitude4, String latitude1, String latitude2, String latitude3, String latitude4) {
        this.parkingSpaceId = id;
        this.label = label;
        this.bookable = booking;
        this.longitude1 = longitude1;
        this.longitude2 = longitude2;
        this.longitude3 = longitude3;
        this.longitude4 = longitude4;
        this.latitude1 = latitude1;
        this.latitude2 = latitude2;
        this.latitude3 = latitude3;
        this.latitude4 = latitude4;
    }


    private int parkingSpaceId;
    private String label;
    private boolean bookable;
    private String longitude1;
    private String longitude2;
    private String longitude3;
    private String longitude4;
    private String latitude1;
    private String latitude2;
    private String latitude3;
    private String latitude4;
}
