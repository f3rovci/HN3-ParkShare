package hr.fer.progi.parkshare.repository;

import hr.fer.progi.parkshare.model.BookingInfo;
import hr.fer.progi.parkshare.model.ParkingSpace;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

import java.sql.Timestamp;
import java.util.List;

public interface BookingInfoRepository extends JpaRepository<BookingInfo, Integer> {
    List<BookingInfo> findByParkingSpace_ParkingSpaceId(int id);

    List<BookingInfo> findAllByParkingSpace(ParkingSpace parkingSpace);

    List<BookingInfo> findAllByStartTimeBetween(
            Timestamp startTime,
            Timestamp endTime);

    List<BookingInfo> findAllByEndTimeBetween(
            Timestamp startTime,
            Timestamp endTime);
}
