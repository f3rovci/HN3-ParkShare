package hr.fer.progi.parkshare.servis.impl;

import hr.fer.progi.parkshare.model.*;
import hr.fer.progi.parkshare.model.dto.BookingInfoDTO;
import hr.fer.progi.parkshare.model.dto.ReserveParkingSpaceDTO;
import hr.fer.progi.parkshare.repository.*;
import hr.fer.progi.parkshare.servis.BookingInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class BookingInfoServiceImpl implements BookingInfoService {


    @Autowired
    private BookingInfoRepository bookingInfoRepository;

    @Autowired
    private ParkingSpaceRepository parkingSpaceRepository;

    @Autowired
    private ParkingLotRepository parkingLotRepository;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public ResponseEntity<List<BookingInfoDTO>> getAvailableParkingSpaces(int parkingSpaceID) {
        ParkingSpace parkingSpace = parkingSpaceRepository.findByParkingSpaceId(parkingSpaceID);
        List<BookingInfo> bookingInfos = bookingInfoRepository.findAllByParkingSpace(parkingSpace);
        List<BookingInfoDTO> bookingInfosDTO = new ArrayList<>();

        for (int i = 0; i < bookingInfos.size(); i++) {
            if (bookingInfos.get(i).isBooked())
                bookingInfosDTO.add(BookingInfoDTO.modelToDTO(bookingInfos.get(i)));
        }

        return new ResponseEntity(bookingInfosDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ParkingSpace>> getAvailableAppointments(Timestamp startTime, Timestamp endTime) {
        List<BookingInfo> bookingInfosStart = bookingInfoRepository.findAllByStartTimeBetween(startTime, endTime);
        List<BookingInfo> bookingInfosEnd = bookingInfoRepository.findAllByEndTimeBetween(startTime, endTime);

        Set<Integer> parkingSpaceIDsStart;
        Set<Integer> parkingSpaceIDsEnd;
        Set<Integer> parkingSpaceIDs = new HashSet<>();

        parkingSpaceIDsStart = bookingInfosStart.stream()
                .map(b -> b.getParkingSpace())
                .map(ps -> ps.getParkingSpaceId())
                .collect(Collectors.toSet());

        parkingSpaceIDsEnd = bookingInfosEnd.stream()
                .map(b -> b.getParkingSpace())
                .map(ps -> ps.getParkingSpaceId())
                .collect(Collectors.toSet());

        parkingSpaceIDs.addAll(parkingSpaceIDsStart);
        parkingSpaceIDs.addAll(parkingSpaceIDsEnd);

        List<ParkingSpace> allParkingSpaces = parkingSpaceRepository.findAll();
        List<ParkingSpace> freeAtGivenInterval;

        freeAtGivenInterval = allParkingSpaces.stream()
                .filter(ps -> !(parkingSpaceIDs.contains(ps.getParkingSpaceId())))
                .collect(Collectors.toList());

        return new ResponseEntity(freeAtGivenInterval, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> reserveParkingSpace(ReserveParkingSpaceDTO reserveDTO) {
        ParkingSpace parkingSpace = parkingSpaceRepository.getById(reserveDTO.getParkingSpaceId());
        AppUser appUser = appUserRepository.findByUsername(reserveDTO.getUsername());

        Timestamp startTime = reserveDTO.getStartTime();
        Timestamp endTime = reserveDTO.getEndTime();

        //(vrijeme u ms)/ms u s/s u min/min u h
        float duration = endTime.getTime()-startTime.getTime();
        duration /= 1000;
        duration /= 60;
        duration /= 60;
        float pricePerDay = getAmount(duration, parkingSpace.getParkingLot());
        float priceForWholeReserv;

        if (reserveDTO.isRepeatWeekly()) {
            priceForWholeReserv = pricePerDay * 4; //repeated 4 times
        } else if (reserveDTO.isRepeatDaily()) {
            priceForWholeReserv = pricePerDay * 5; //repeated 5 times
        } else {
            priceForWholeReserv = pricePerDay;
        }

        float balanceAfterReserv = appUser.getWalletBalance() - priceForWholeReserv;
        if (balanceAfterReserv < 0) {
            return new ResponseEntity<>("Not enough money.", HttpStatus.CONFLICT);
        }

        return repeatReservations(parkingSpace, appUser, reserveDTO.isRepeatDaily(), reserveDTO.isRepeatWeekly(),
                startTime, endTime);
    }

    /**
     * Functions makes repeating reservations for parking spaces.
     * It is possible to make repetitions per day or per week.
     * */
    private ResponseEntity repeatReservations(ParkingSpace parkingSpace, AppUser appUser,
                                              boolean repeatDaily, boolean repeatWeakly, Timestamp startTime, Timestamp endTime) {
        //get wanting time for reservation in calendar
        Calendar calStart = Calendar.getInstance();
        calStart.setTime(startTime);

        //see if user wants to repeat his reservation
        if (repeatWeakly) {
            calStart.add(Calendar.DAY_OF_WEEK, 21); //this week + 3 more = 4 weeks total
        } else if (repeatDaily) {
            calStart.add(Calendar.DAY_OF_WEEK, 4); //this day + 4 more = 5 days total
        }

        //define last day of repeating reservations
        Timestamp lastDayStartTime = new Timestamp(calStart.getTime().getTime());

        String unsuccessfullyReserved = "";
        int counter = 0;
        while (startTime.before(lastDayStartTime) || startTime.equals(lastDayStartTime)) {
            calStart.setTime(startTime);
            System.out.println(counter + " " + repeatDaily);
            try {
                reserve(parkingSpace, appUser, startTime, endTime);
            } catch (IllegalArgumentException e) {
                unsuccessfullyReserved = unsuccessfullyReserved + e.getMessage();
            }
            counter++;
            if (repeatWeakly) {
                startTime = new Timestamp(startTime.getTime() + 24*60*60*1000*7);
                endTime = new Timestamp(endTime.getTime() + 24*60*60*1000*7);
            } else if (repeatDaily) {
                startTime = new Timestamp(startTime.getTime() + 24*60*60*1000);
                endTime = new Timestamp(endTime.getTime() + 24*60*60*1000);
            } else {
                break;
            }
        }

        return new ResponseEntity<>(unsuccessfullyReserved, HttpStatus.OK);
    }

    /**
     * Method does an actual reservation for a parking space at specific interval.
     * */
    private void reserve(ParkingSpace parkingSpace, AppUser appUser, Timestamp startTime, Timestamp endTime) {
        //find if there are reservations made at that time
            //get all reservations for that parking space
        List<BookingInfo> bookingInfos = bookingInfoRepository.findAllByParkingSpace(parkingSpace);

        bookingInfos = bookingInfos.stream()
                .filter(b -> (b.getStartTime().before(startTime) && b.getEndTime().after(startTime)) ||
                        (b.getStartTime().before(endTime) && b.getEndTime().after(endTime)) ||
                        (b.getStartTime().equals(startTime) || b.getEndTime().equals(endTime)))
                .collect(Collectors.toList());

        //ako sada bookingInfos imaju elemenata znaci da je tada parking zauzet
        if (!bookingInfos.isEmpty())
            throw new IllegalArgumentException(";"+startTime+","+endTime+";");

        //milisekunde
        float duration = endTime.getTime()-startTime.getTime();
        //sekunde
        duration = duration/1000;
        //minute
        duration = duration/60;
        //sati
        duration = duration/60;

        float famount = getAmount(duration, parkingSpace.getParkingLot());

        if(appUser.getWalletBalance() < famount){
            throw new IllegalArgumentException("Please update your wallet!");
        }
        appUser.setWalletBalance(appUser.getWalletBalance() - famount);

        Date dateDay = new Date();
        Transaction transaction = new Transaction(dateDay,famount,appUser);

        transactionRepository.save(transaction);

        BookingInfo bookingInfo = new BookingInfo(startTime, endTime,
                true, parkingSpace, appUser, null);

        try {
            bookingInfoRepository.save(bookingInfo);
        } catch(IllegalArgumentException e) {
            throw new IllegalArgumentException(";"+startTime+","+endTime+";");
        }

        System.out.println("Successful reservation");
    }

    /**
     * Method calculates price for parking reservation.
     *
     * @param duration float
     * @param parkingLot {@link ParkingLot}
     *
     * @return float
     *
     * @throws IllegalArgumentException
     * */
    private float getAmount(float duration, ParkingLot parkingLot) {
        if(duration >= 10) {
            return parkingLot.getPrice10()*duration;
        }
        else if(duration >= 5) {
            return parkingLot.getPrice5()*duration;
        }
        else if(duration >=0){
            return parkingLot.getPrice1()*duration;
        }
        else{
            throw new IllegalArgumentException();
        }
    }

    @Override
    public ResponseEntity<String> occupyParkingSpace(Integer parkingSpaceId) {
        ParkingSpace parkingSpace = parkingSpaceRepository.getById(parkingSpaceId);
        List<BookingInfo> bookingInfos = bookingInfoRepository.findAllByParkingSpace(parkingSpace);

        //Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        Timestamp currentTime = new Timestamp(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(1));


        bookingInfos = bookingInfos.stream()
                .filter(b -> (b.getStartTime().before(currentTime) && b.getEndTime().after(currentTime)) ||
                        (b.getStartTime() != null && b.getEndTime() == null))
                .collect(Collectors.toList());

        if (!bookingInfos.isEmpty()) {
            return new ResponseEntity("Parking space currently occupied or is reserved.", HttpStatus.CONFLICT);
        }

        BookingInfo bookingInfo = new BookingInfo(currentTime, null, false, parkingSpace,
                null, null);

        bookingInfoRepository.save(bookingInfo);

        return new ResponseEntity<>("Successfully occupied.", HttpStatus.OK);

    }

    @Override
    public ResponseEntity<String> freeParkingSpace(Integer parkingSpaceId) {
        ParkingSpace parkingSpace = parkingSpaceRepository.getById(parkingSpaceId);
        List<BookingInfo> bookingInfos = bookingInfoRepository.findAllByParkingSpace(parkingSpace);

        Timestamp currentTime = new Timestamp(System.currentTimeMillis());

        bookingInfos = bookingInfos.stream()
                .filter(b -> (b.getStartTime() != null && b.getEndTime() == null))
                .collect(Collectors.toList());

        if (bookingInfos.isEmpty()) {
            return new ResponseEntity("Parking space was not occupied", HttpStatus.BAD_REQUEST);
        }

        if (bookingInfos.size() > 1) {
            return new ResponseEntity("This spot has multiple reservations.", HttpStatus.CONFLICT);
        }

        BookingInfo bookingInfo = bookingInfos.get(0);

        bookingInfo.setEndTime(currentTime);

        bookingInfoRepository.save(bookingInfo);

        return new ResponseEntity<>("Successfully freed.", HttpStatus.OK);

    }

    @Override
    public ResponseEntity<String> occupyBikeSpace(Integer parkingLotId) {
        ParkingLot parkingLot = parkingLotRepository.getById(parkingLotId);

        int currentBikes = parkingLot.getCurrentBikes();

        if (currentBikes < 1)
            return new ResponseEntity<>("No bike parking available.", HttpStatus.OK);

        parkingLot.setCurrentBikes(--currentBikes);

        parkingLotRepository.save(parkingLot);

        return new ResponseEntity<>("Bike parking successfully occupied.", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> freeBikeSpace(Integer parkingLotId) {
        ParkingLot parkingLot = parkingLotRepository.getById(parkingLotId);

        int currentBikes = parkingLot.getCurrentBikes();

        if (currentBikes == parkingLot.getMaxBikes())
            return new ResponseEntity<>("All bike parking are free. Cannot free a bike parking.", HttpStatus.BAD_REQUEST);

        parkingLot.setCurrentBikes(++currentBikes);

        parkingLotRepository.save(parkingLot);

        return new ResponseEntity<>("Bike parking successfully freed.", HttpStatus.OK);
    }
}
