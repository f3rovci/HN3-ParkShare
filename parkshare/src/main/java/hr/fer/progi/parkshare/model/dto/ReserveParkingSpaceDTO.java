package hr.fer.progi.parkshare.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.sql.Timestamp;

@AllArgsConstructor
@Getter
public class ReserveParkingSpaceDTO {
    private Timestamp startTime;
    private Timestamp endTime;
    private Integer parkingSpaceId;
    private String username;
    private boolean repeatDaily;
    private boolean repeatWeekly;

}
