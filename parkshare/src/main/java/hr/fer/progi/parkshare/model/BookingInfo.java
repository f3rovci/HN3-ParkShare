package hr.fer.progi.parkshare.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class BookingInfo {

    public BookingInfo(@NonNull Timestamp startTime, Timestamp endTime, boolean booked, @NonNull ParkingSpace parkingSpace, AppUser appUser, Transaction transaction) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.booked = booked;
        this.parkingSpace = parkingSpace;
        this.appUser = appUser;
        this.transaction = transaction;
    }

    @Id
    @GeneratedValue
    @Column
    private Integer bookingInfoId;

    @Column
    @NonNull
    private Timestamp startTime;

    @Column
    private Timestamp endTime;

    @Column
    private boolean booked;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "parking_space_id", nullable = false)
    private ParkingSpace parkingSpace;

    @ManyToOne
    @JoinColumn(name = "username", nullable = true)
    private AppUser appUser;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "transaction_id", referencedColumnName = "transactionId")
    private Transaction transaction;
}
