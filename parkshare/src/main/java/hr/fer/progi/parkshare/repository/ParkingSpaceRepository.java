package hr.fer.progi.parkshare.repository;

import hr.fer.progi.parkshare.model.ParkingSpace;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ParkingSpaceRepository extends JpaRepository<ParkingSpace, Integer> {
    ParkingSpace findByParkingSpaceId(int id);

    List<ParkingSpace> findByParkingLot_parkingLotId(int id);
}
