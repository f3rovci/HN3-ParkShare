package hr.fer.progi.parkshare.controller;

import hr.fer.progi.parkshare.model.ParkingSpace;
import hr.fer.progi.parkshare.model.dto.*;
import hr.fer.progi.parkshare.servis.impl.AppUserServiceImpl;
import hr.fer.progi.parkshare.servis.impl.ManagerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@CrossOrigin
@Controller
@RequestMapping("/api")
public class RootController {

    @Autowired
    private AppUserServiceImpl userService;

    @Autowired
    private ManagerServiceImpl managerService;

    @PostMapping("/register")
    public ResponseEntity<Map<String,String>> registerUser(@Valid @RequestPart("username") String username, @RequestPart("password") String password,
                                                           @RequestPart("firstName") String firstName, @RequestPart("lastName") String lastName,
                                                           @RequestPart("iban") String iban, @RequestPart("email") String email, @RequestPart("image") MultipartFile image, @RequestPart("role") String role) throws Exception {
        try {
            AppUserDTO appUser = new AppUserDTO(username, password, firstName, lastName, iban, image, email, role);
            return userService.register(appUser);
        }catch (Exception e){
            return  new ResponseEntity<Map<String, String>>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/confirm-account")
    public ResponseEntity<String> confirmUser(@RequestParam String token) {
        return userService.verifyToken(token);
    }

    @PostMapping("/view-parking-space")
    public ResponseEntity<List<ParkingSpaceReturnDTO>> getEveryParkingSpace(@RequestPart("id") String id) {
        try {
            return managerService.getEveryParkingSpace(Integer.parseInt(id));
        } catch (Exception e) {
            return new ResponseEntity<List<ParkingSpaceReturnDTO>>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/view-all-parking-lots")
    public ResponseEntity<List<ParkingLotDTO>> getAllParkingLots() {

        List<ParkingLotDTO> ret = null;
        ret = userService.getAllParkingLotsDTO();

        return new ResponseEntity<List<ParkingLotDTO>>(ret, HttpStatus.OK);
    }

    @GetMapping("/view-all-parking-spaces")
    public ResponseEntity<List<CarParkingSpaceDTO>> getAllParkingSpaces() {

        List<CarParkingSpaceDTO> ret = null;
        ret = userService.getParkingSpacesDTO();

        return new ResponseEntity<List<CarParkingSpaceDTO>>(ret, HttpStatus.OK);
    }


}
