package hr.fer.progi.parkshare.servis.impl;

import hr.fer.progi.parkshare.model.AppUser;
import hr.fer.progi.parkshare.model.allenum.Roles;
import hr.fer.progi.parkshare.model.dto.AdminViewUserDTO;
import hr.fer.progi.parkshare.model.dto.ManagerDTO;
import hr.fer.progi.parkshare.repository.AppUserRepository;
import hr.fer.progi.parkshare.servis.AdminService;
import org.apache.commons.validator.routines.IBANValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    AppUserRepository userRepository;

    @Override
    public List<AdminViewUserDTO> getRegisteredUsers() {
        List<AdminViewUserDTO> listRegistered = new ArrayList<>();
        List<AppUser> listAll = userRepository.findAll();

        for (int i = 0; i < listAll.size(); i++) {
            if (listAll.get(i).getRole().equals(Roles.CLIENT) || listAll.get(i).getRole().equals(Roles.PARKING_MANAGER) || listAll.get(i).getRole().equals(Roles.ADMIN)) {
                AdminViewUserDTO adminViewUserDTO = new AdminViewUserDTO();
                adminViewUserDTO.setUsername(listAll.get(i).getUsername());
                adminViewUserDTO.setFirstName(listAll.get(i).getFirstName());
                adminViewUserDTO.setLastName(listAll.get(i).getLastName());
                adminViewUserDTO.setIBAN(listAll.get(i).getIBAN());
                adminViewUserDTO.setImage(listAll.get(i).getImage());
                adminViewUserDTO.setEmail(listAll.get(i).getEmail());
                adminViewUserDTO.setRole(listAll.get(i).getRole());
                listRegistered.add(adminViewUserDTO);
            }
        }
        return listRegistered;
    }

    @Override
    public ResponseEntity<String> promoteManager(String username) {
        ResponseEntity<String> responseEntity;
        AppUser user = userRepository.findByUsername(username);
        if(user != null && user.getRole().equals(Roles.UNAPPROVED)) {
            user.setRole(Roles.PARKING_MANAGER);
            userRepository.save(user);
            String ret = "You successfully promoted " + user.getUsername();
            responseEntity = new ResponseEntity<String>(ret,HttpStatus.OK);
        }else
            responseEntity = new ResponseEntity<String>("Parking manager with that username doesn't exist!",HttpStatus.BAD_REQUEST);
        return responseEntity;
    }

    @Override
    public ResponseEntity<String> editAppUserInfo(AdminViewUserDTO userToEdit)  {
        IBANValidator validator = new IBANValidator();
        HttpStatus status = HttpStatus.OK;
        String message = new String();
        AppUser user = userRepository.findByUsername(userToEdit.getUsername());

        if(!validator.isValid(userToEdit.getIBAN())){
            message = "IBAN is not valid.";
            return new ResponseEntity(message, HttpStatus.BAD_REQUEST);
        }

        if ((user != null) && (user.getEmail().equals(userToEdit.getEmail()))) {
            user.setFirstName(userToEdit.getFirstName());
            user.setLastName(userToEdit.getLastName());
            user.setIBAN(userToEdit.getIBAN());
            user.setImage(userToEdit.getImage());
            user.setRole(userToEdit.getRole());
            userRepository.save(user);
            message = "User data successfully changed.";
        } else {
            message = "User does not exist or has a different email.";
            status = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity(message, status);

    }

    @Override
    public ResponseEntity<String> promoteAdmin(String username) {
        ResponseEntity<String> responseEntity;
        AppUser user = userRepository.findByUsername(username);
        if(user != null && (user.getRole().equals(Roles.PARKING_MANAGER) || user.getRole().equals(Roles.CLIENT))) {
            user.setRole(Roles.ADMIN);
            userRepository.save(user);
            String ret = "You successfully promoted " + user.getUsername();
            responseEntity = new ResponseEntity<String>(ret,HttpStatus.OK);
        }else
            responseEntity = new ResponseEntity<String>("User with that username doesn't exist!",HttpStatus.BAD_REQUEST);
        return responseEntity;
    }

}
