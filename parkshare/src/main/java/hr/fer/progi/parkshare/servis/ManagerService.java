package hr.fer.progi.parkshare.servis;
import hr.fer.progi.parkshare.model.ParkingLot;
import hr.fer.progi.parkshare.model.ParkingSpace;
import hr.fer.progi.parkshare.model.dto.*;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ManagerService {
    ResponseEntity<String> editParkingLotInfo(ParkingLotEditDTO parkingLotEditDTO);
    ResponseEntity<List<ParkingSpaceReturnDTO>> getEveryParkingSpace(int parkingLotId);
    ResponseEntity<List<ParkingSpaceReturnDTO>> getAvailableParkingSpace(int parkingLotId);
    ResponseEntity<String> defineReservability(int id, boolean bookable);
    ResponseEntity<Boolean> createParkingLot(ParkingLot parkingLot);
    ResponseEntity<Map<String, String>> createParkingSpace(ParkingSpace parkingSpace);
    ResponseEntity<ParkingLotReturnDTO> showParkingLotsFromManager(String username);
    ResponseEntity<Boolean> deleteParkingSpace(int parkingSpaceId);
    ResponseEntity<List<ParkingSpaceDTO>> getParkingSpacesManager(int parkingLotId);
    ResponseEntity<String> editParkingSpaceCoordinates(int id, String longitude1, String longitude2, String longitude3, String longitude4, String latitude1, String latitude2, String latitude3, String latitude4);
    ResponseEntity<List<Integer>> getStatisticsLast7(String username, LocalDate date);

}
