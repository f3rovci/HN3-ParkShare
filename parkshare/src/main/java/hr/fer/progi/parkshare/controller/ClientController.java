package hr.fer.progi.parkshare.controller;

import hr.fer.progi.parkshare.model.ParkingSpace;
import hr.fer.progi.parkshare.model.dto.*;
import hr.fer.progi.parkshare.servis.impl.AppUserServiceImpl;
import hr.fer.progi.parkshare.servis.impl.BookingInfoServiceImpl;
import hr.fer.progi.parkshare.servis.impl.ManagerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

@CrossOrigin
@Controller
@RequestMapping("/api/client")
public class ClientController {
    @Autowired
    private AppUserServiceImpl userService;

    @Autowired
    private ManagerServiceImpl managerService;

    @Autowired
    private BookingInfoServiceImpl bookingInfoService;

    @GetMapping("/user/{username}")
    public ResponseEntity<UserViewDTO> userData(@PathVariable String username){
        return new ResponseEntity<UserViewDTO>(userService.userData(username), HttpStatus.OK);
    }

    @PostMapping("/money-transfer")
    public ResponseEntity<Map<String, String>> updateWallet(@RequestPart("amount") String amount, @RequestPart("username") String username){
        float famount = Float.valueOf(amount);
        return new ResponseEntity<Map<String, String>>(userService.updateWallet(famount, username), HttpStatus.OK);
    }

    @PostMapping("/getCarParking")
    public ResponseEntity<CarParkingSpaceDTO> getParkingSpace(@Valid @RequestPart("longitude") String longitude, @RequestPart("latitude") String latitude) {

        CarParkingSpaceDTO ret ;
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        ts = new Timestamp(ts.getTime() + TimeUnit.HOURS.toMillis(1));

        ret = userService.findClosestCar(Float.parseFloat(longitude),
                Float.parseFloat(latitude), ts);


        return new ResponseEntity<>(ret, HttpStatus.OK);
    }

    @PostMapping("/getBikeParking")
    public ResponseEntity<Map<String,String>> getBikeParking(@Valid @RequestPart("longitude") String longitude, @RequestPart("latitude") String latitude) {

        Map<String, String> ret = null;

        ret = userService.findClosestBike(Float.parseFloat(longitude), Float.parseFloat(latitude));


        return new ResponseEntity<Map<String, String>>(ret, HttpStatus.OK);
    }


    @PostMapping("/view-parking-space-status")
    public ResponseEntity<List<ParkingSpaceReturnDTO>> getAvailableParkingSpace(@RequestPart("id") String id) {
        try {
            return managerService.getAvailableParkingSpace(Integer.parseInt(id));
        } catch (Exception e) {
            return new ResponseEntity<List<ParkingSpaceReturnDTO>>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/clientview-parking-lots")
    public ResponseEntity<List<ParkingLotReturnDTO>> clientViewParkingLots() {
        try {
            return userService.clientViewParkingLots();
        } catch (Exception e) {
            return new ResponseEntity<List<ParkingLotReturnDTO>>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/view-availble-parking-spaces")
    public ResponseEntity<List<ParkingSpaceDTO>> getAvailbleParkingSpaceDTO(){

        List<ParkingSpaceDTO> ret = null;
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        ts = new Timestamp(ts.getTime() + TimeUnit.HOURS.toMillis(1));
        ret = userService.getAvailableParkingSpacesDTO(ts);

        return new ResponseEntity<List<ParkingSpaceDTO>>(ret, HttpStatus.OK);
    }

    /**
     * Endpoint is used when a client wants to
     * occupy or free a bike space.
     *
     * @param parkingLotId {@link Integer}
     * @param occupy boolean
     *
     * @return ResponseEntity with {@link String}
     * */
    @PostMapping("/manage-bike-space")
    public ResponseEntity<String> occupyBikeSpace(
            @RequestParam Integer parkingLotId, @RequestParam boolean occupy
    ) {
        if (occupy)
            return bookingInfoService.occupyBikeSpace(parkingLotId);
        else
            return bookingInfoService.freeBikeSpace(parkingLotId);
    }
}
