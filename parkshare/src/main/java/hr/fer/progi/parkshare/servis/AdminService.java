package hr.fer.progi.parkshare.servis;

import hr.fer.progi.parkshare.model.AppUser;
import hr.fer.progi.parkshare.model.dto.AdminViewUserDTO;
import hr.fer.progi.parkshare.model.dto.AppUserDTO;
import hr.fer.progi.parkshare.model.dto.ManagerDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AdminService {
    List<AdminViewUserDTO> getRegisteredUsers();
    ResponseEntity<String> promoteManager(String username);
    ResponseEntity<String> editAppUserInfo(AdminViewUserDTO userToEdit);

    ResponseEntity<String> promoteAdmin(String username);
}
