package hr.fer.progi.parkshare;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class SystemTesting {
    WebDriver driver;

    @BeforeEach
    public void init() {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://f3rovci-parkshare-frontend.herokuapp.com/");
    }

    @Test
    public void testUserLoginSuccessful() {
        driver.findElement(By.cssSelector("button:nth-child(1)")).click();

        WebElement element = driver.findElement(By.id("username"));
        element.sendKeys("ivan");

        element = driver.findElement(By.id("password"));
        element.sendKeys("password");

        driver.findElement(By.id("loginInputButton")).click();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String redirURL = driver.getCurrentUrl();
        boolean resActual = redirURL.contains("home");

        assertTrue(resActual);
    }

    @Test
    public void testFindClosestParkingSpaceSuccessfully() {
        driver.findElement(By.cssSelector("button:nth-child(1)")).click();

        WebElement element = driver.findElement(By.id("username"));
        element.sendKeys("ivan");

        element = driver.findElement(By.id("password"));
        element.sendKeys("password");

        driver.findElement(By.id("loginInputButton")).click();
        driver.findElement(By.id("toggleShowParkingsButton")).click();
        driver.findElement(By.id("findClosestButton")).click();

        element = driver.findElement(By.id("map"));
        element.click();
        element.click();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertDoesNotThrow(() -> driver.findElement(By.className("leaflet-routing-container")));
    }

    @Test
    public void testOccupyBikeSpaceAsManager() {
        driver.findElement(By.cssSelector("button:nth-child(1)")).click();

        WebElement element = driver.findElement(By.id("username"));
        element.sendKeys("lini");

        element = driver.findElement(By.id("password"));
        element.sendKeys("password");

        driver.findElement(By.id("loginInputButton")).click();
        driver.findElement(By.id("occupyBikeSpaceButton")).click();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertTrue(isAlertPresent());
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException Ex) {
            return false;
        }
    }

    @Test
    public void testAddMoneyToAccount() {
        driver.findElement(By.cssSelector("button:nth-child(1)")).click();
        WebElement element = driver.findElement(By.id("username"));
        element.sendKeys("ivan");
        element = driver.findElement(By.id("password"));
        element.sendKeys("password");
        driver.findElement(By.id("loginInputButton")).click();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.cssSelector(".links > button:nth-child(1)")).click();
        driver.findElement(By.id("button")).click();
        driver.findElement(By.id("input1")).click();
        driver.findElement(By.id("input1")).sendKeys(""+5);
        driver.findElement(By.id("button")).click();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertThrows(NoSuchElementException.class, () -> driver.findElement(By.className("showInput")).click());
    }
}
