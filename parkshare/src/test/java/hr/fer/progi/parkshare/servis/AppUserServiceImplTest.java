package hr.fer.progi.parkshare.servis;

import static org.junit.jupiter.api.Assertions.*;

import hr.fer.progi.parkshare.model.AppUser;
import hr.fer.progi.parkshare.model.allenum.Roles;
import hr.fer.progi.parkshare.servis.impl.AppUserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("local")
@SpringBootTest
public class AppUserServiceImplTest {

    @Autowired
    private AppUserServiceImpl userService;

    @Test
    public void testCreateUserThrowsExceptionWhenCreatingExistingUser() {
        AppUser user1 = new AppUser("username112", "password", "john", "wick",
                "iban", "image", "email112", Roles.UNAPPROVED, 0);

        userService.createUser(user1);

        assertThrows(IllegalArgumentException.class, () -> userService.createUser(user1));
    }

    @Test
    public void testUpdateWallet() {
        AppUser user1 = new AppUser("username100", "password", "john", "wick",
                "iban", "image", "email100", Roles.CLIENT, 0);
        userService.createUser(user1);

        userService.updateWallet(100, user1.getUsername());

        float walletActually = userService.getUser(user1.getEmail()).getWalletBalance();

        assertEquals((float) 100, walletActually);
    }

}

