package hr.fer.progi.parkshare.servis;

import static org.junit.jupiter.api.Assertions.*;

import hr.fer.progi.parkshare.model.AppUser;
import hr.fer.progi.parkshare.model.ParkingLot;
import hr.fer.progi.parkshare.model.ParkingSpace;
import hr.fer.progi.parkshare.model.allenum.Roles;
import hr.fer.progi.parkshare.servis.impl.BookingInfoServiceImpl;
import hr.fer.progi.parkshare.servis.impl.ManagerServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.Map;

@ActiveProfiles("local")
@SpringBootTest
public class BookingInfoServiceImplTest {

    @Autowired
    BookingInfoServiceImpl bookingInfoService;

    @Autowired
    ManagerServiceImpl managerService;

    ParkingSpace parkingSpace;
    String psId;

    @Test
    public void testOccupyParkingSpace() {
        AppUser user1 = new AppUser("username1126", "password", "john", "wick",
                "iban", "image", "email1126", Roles.UNAPPROVED, 0);

        ParkingLot parkingLot = new ParkingLot(
                3, 3, "name", "description", "3.0", "4.0",
                "5.0", "6.0", "3.0", "4.0",
                "5.0", "6.0", user1, "image",1, 2, 3
        );

        managerService.createParkingLot(parkingLot);

        parkingSpace = new ParkingSpace(
                "label", true, "1.00", "1.01", "1.02", "1.03",
                "1.00", "1.01", "1.02", "1.03",
                parkingLot
        );

        ResponseEntity<Map<String, String>> re = managerService.createParkingSpace(parkingSpace);

        Map<String, String> map = re.getBody();

        psId = map.get("parkingSpaceId");

        Integer parkingSpaceId = Integer.parseInt(psId);

        ResponseEntity<String> re1 = bookingInfoService.occupyParkingSpace(parkingSpaceId);

        assertEquals(HttpStatus.OK, re1.getStatusCode());
    }

    @Test
    public void testFreeParkingSpaceReturnsBadRequestWhenFreeingAlreadyFreeParkingSpace() {
        AppUser user1 = new AppUser("username1125", "password", "john", "wick",
                "iban", "image", "email1125", Roles.UNAPPROVED, 0);

        ParkingLot parkingLot = new ParkingLot(
                3, 3, "name", "description", "3.0", "4.0",
                "5.0", "6.0", "3.0", "4.0",
                "5.0", "6.0", user1, "image",1, 2, 3
        );

        managerService.createParkingLot(parkingLot);

        parkingSpace = new ParkingSpace(
                "label", true, "1.00", "1.01", "1.02", "1.03",
                "1.00", "1.01", "1.02", "1.03",
                parkingLot
        );

        ResponseEntity<Map<String, String>> re = managerService.createParkingSpace(parkingSpace);

        Map<String, String> map = re.getBody();

        psId = map.get("parkingSpaceId");

        Integer parkingSpaceId = Integer.parseInt(psId);

        ResponseEntity<String> re1 = bookingInfoService.freeParkingSpace(parkingSpaceId);

        assertEquals(HttpStatus.BAD_REQUEST, re1.getStatusCode());
    }
}

