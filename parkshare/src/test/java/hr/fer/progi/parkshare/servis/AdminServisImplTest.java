package hr.fer.progi.parkshare.servis;

import static org.junit.jupiter.api.Assertions.*;

import hr.fer.progi.parkshare.model.AppUser;
import hr.fer.progi.parkshare.model.allenum.Roles;
import hr.fer.progi.parkshare.model.dto.AdminViewUserDTO;
import hr.fer.progi.parkshare.servis.impl.AdminServiceImpl;
import hr.fer.progi.parkshare.servis.impl.AppUserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

@ActiveProfiles("local")
@SpringBootTest
public class AdminServisImplTest {

    @Autowired
    AdminServiceImpl adminService;

    @Autowired
    AppUserServiceImpl appUserService;

    @Test
    public void testGetRegisteredUsersReturnsAllRegisteredUsers() {
        AppUser user1 = new AppUser("username113", "password", "john", "wick",
                "iban", "image", "email123", Roles.PARKING_MANAGER, 0);
        appUserService.createUser(user1);

        AppUser user2 = new AppUser("username12", "password", "john",
                "wick", "iban", "image", "email233", Roles.CLIENT, 0);
        appUserService.createUser(user2);

        List<AdminViewUserDTO> allUsersDTO = adminService.getRegisteredUsers();

        AdminViewUserDTO user1DTOActual = null;
        AdminViewUserDTO user2DTOActual = null;

        for (AdminViewUserDTO dto : allUsersDTO) {
            if (dto.getEmail().equals(user1.getEmail()))
                user1DTOActual = dto;
            if (dto.getEmail().equals(user2.getEmail()))
                user2DTOActual = dto;
        }

        assertFalse(allUsersDTO.isEmpty());
        assertTrue(user1DTOActual.getUsername().equals(user1.getUsername()));
        assertTrue(user2DTOActual.getUsername().equals(user2.getUsername()));
    }

    @Test
    public void testPromotingManager() {
        AppUser user1 = new AppUser("username123", "password", "john", "wick",
                "iban", "image", "email121", Roles.UNAPPROVED, 0);
        appUserService.createUser(user1);

        ResponseEntity re = adminService.promoteManager("username123");
        assertEquals(HttpStatus.OK, re.getStatusCode());

        AppUser user1Actual = appUserService.getUser("email121");
        assertEquals(Roles.PARKING_MANAGER, user1Actual.getRole());
    }
}
