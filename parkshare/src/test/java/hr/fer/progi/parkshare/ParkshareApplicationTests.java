package hr.fer.progi.parkshare;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("local")
@SpringBootTest
class ParkshareApplicationTests {

	@Test
	void contextLoads() {
		assertTrue(true);
	}

}
