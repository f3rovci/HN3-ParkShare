import '../styles/register.css'
import { useState } from 'react';
import $ from 'jquery';

const Register = ({ setMsg, domain }) => {
    var title = "Register";

    //WE NEED TO ADD IBAN AND IMAGE
    const [inputs, setInputs] = useState({ role: "CLIENT", username: "", firstName: "", lastName: "", email: "", password: "", iban: "", image: "" });
    const [responseMsg, setResponse] = useState('');

    const handleChange = (event) => {

        $('.pane').css('height', '50rem');
        setResponse('');

        //load name atribute and value to inputs object
        const name = event.target.name;
        var value = event.target.value;

        setInputs(values => ({ ...values, [name]: value }));
    }

    const handleSubmit = async (event) => {
        try {
            event.preventDefault();

            //handle image
            const image = document.getElementById("image");
            inputs.image = image.files[0];

            const formData = new FormData();

            for (var key in inputs) {
                formData.append(key, inputs[key]);
            }

            console.log(inputs.image)

            const url = `${domain}api/register`;

            // Handling post request
            const response = await fetch(url, {
                method: 'POST',
                body: formData
            });

            //console.log("res")
            //console.log(response);

            if (!response.ok) {
                setResponse('Image size is to big')
                //throw Error('Image size is to big');
            } else {
                console.log(response.status);
                const data = await response.json();

                if (data.boolean === "true") {
                    setMsg(data.message);
                    setResponse('');
                    $('#popupRegister').hide();
                    $('#popupLogin').show();
                } else {
                    setMsg('');
                    $('.pane').css('height', '52rem');
                    setResponse(data.message);
                }
                console.log(data);
            }


        } catch (e) {
            console.error(e);
        }
    }

    //hide popup when you click outside of graph
    /*
    $(document).mouseup(function (e) {
        var container = $("#popupRegister");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('#popupRegister').hide();
        }
    });
    */

    //hide popup with ESC button
    $(document).on("keydown", function (e) {
        if (e.keyCode === 27) {
            $('#popupRegister').hide();
        }
    });

    return (
        <div className="popupDark" id="popupRegister">
            <div className="register">
                <div className="pane">
                    <div className="titlePart">{title}</div>

                    <div className="formPart">
                        <form className="registerForm" onSubmit={handleSubmit}>
                            <select className="inputField" name="role" onChange={handleChange}>
                                <option value="CLIENT">Client</option>
                                <option value="PARKING_MANAGER" >Parking chief</option>
                            </select>

                            <input className="registerInput" type="text" className="inputField" name="username" id="username" placeholder=" Username.." required minLength="4" onChange={handleChange} />
                            <label className="registerLabel" htmlFor="username"></label>

                            <input className="registerInput" type="text" className="inputField" name="firstName" id="name" placeholder=" Your name.." required onChange={handleChange} />

                            <input className="registerInput" type="text" className="inputField" name="lastName" id="lastname" placeholder=" Your lastname.." required onChange={handleChange} />

                            <input className="registerInput" type="email" className="inputField" name="email" id="email" placeholder=" Email.." required onChange={handleChange} />

                            <input className="registerInput" type="password" className="inputField" name="password" id="password" placeholder=" Password.." required minLength="6" onChange={handleChange} />

                            <input className="registerInput" type="text" className="inputField" name="iban" id="IBAN" placeholder=" Your IBAN.." required onChange={handleChange} />
                            <input className="registerInput" type="file" className="imgField" name="image" id="image" accept="image/*" required />
                            <label className="registerLabel" htmlFor="image" className="custom-file-upload">
                                Upload ID image
                            </label>

                            <input className="registerInput" type="submit" name="" id="submit" value="Register" />
                            <label htmlFor="" className="error-msg">{responseMsg}</label>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default Register;

/*Register 1*/
/*
            <div className="register">
                <div className="pane">
                    <div className="titlePart">{title}</div>
                    <div className="formPart"></div>
                </div>
            </div>
 */

/*Register 2*/
/*
<div className="registerV2">
                <div className="paneV2">
                    <div className="titlePartV2">
                        <h1>{title}</h1>
                    </div>

                    <div className="formPartV2">
                        <hr />
                        <form action="" method="POST">

                            <select id="cars" className="inputField" required>
                                <option value="" disabled selected>Register as?</option>
                                <option value="volvo">Client</option>
                                <option value="saab">Parking chief</option>
                            </select>

                            <input type="text" className="inputField" name="username" id="username" placeholder=" Username.." required />

                            <input type="text" className="inputField" name="name" id="name" placeholder=" Your name.." required />

                            <input type="text" className="inputField" name="surname" id="surname" placeholder=" Your surname.." required />

                            <input type="email" className="inputField" name="email" id="email" placeholder=" Email.." />

                            <input type="password" className="inputField" name="password" id="password" placeholder=" Password.." required />

                            <input type="text" className="inputField" name="IBAN" id="IBAN" placeholder=" Your IBAN.." required />
                            <input type="file" className="imgField" name="image" id="image" required />
                            <label htmlFor="image" className="custom-file-upload">
                                ID image
                            </label>
                            <input type="submit" name="" id="submit" value="Submit" />
                        </form>
                    </div>
                    </div>
            </div>
*/