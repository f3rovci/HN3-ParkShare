import UserList from "./adminComponents/UserList";
import UnaprovedManagers from "./adminComponents/UnaprovedManagers";
import jws from 'jws';
import ClientInfo from "./userComponents/ClientInfo";
import CreateParking from "./managerComponents/CreateParking";
import CreateParkingSpace from "./managerComponents/CreateParkingSpace";
import EditParkingSpace from "./managerComponents/EditParkingSpace";
import EditParking from "./managerComponents/EditParking";
import Calendar from "./Calendar.js"
import Statistics from "./managerComponents/Statistics"
import ParkingSpotOccupation from "./managerComponents/ParkingSpotOccupation";

const Home = ({
    showCreateParkingWindow, setShowCreateParkingWindow, pData,
    showEditParkingSpaceWindow, setShowEditParkingSpaceWindow,
    showEditParkingWindow, setShowEditParkingWindow, epData,
    showCreateParkingSpaceWindow, setShowCreateParkingSpaceWindow, psData,
    setParkingSpots,
    setShowCalendarWindow, showCalendarWindow, calendarParkingId,
    setParkingOccupationWindow, showParkingOccupationWindow,
    setParkingSpot, parkingSpot,
    showUserInfoWindow,
    domain
}) => {

    if (!(localStorage.getItem('token'))) {
        alert("You are not authorized");
        window.location.replace('/')
    }

    //console.log((jws.decode(localStorage.getItem('token')).payload.exp > seconds));

    var date = new Date();
    var seconds = date.getTime() / 1000;
    //console.log(seconds)
    if ((jws.decode(localStorage.getItem('token')).payload.exp < seconds)) {
        localStorage.removeItem('token')
        window.location.replace('/')
    }


    //mozda jos dodati .includes(parking manager) za ClientInfo
    return (<div className="home">
        <div className="cover" id='cover'></div>
        {jws.decode(localStorage.getItem('token')).payload.role.includes('ADMIN') && <UserList domain={domain} />}
        {jws.decode(localStorage.getItem('token')).payload.role.includes('ADMIN') && <UnaprovedManagers domain={domain} />}
        {jws.decode(localStorage.getItem('token')).payload.role.includes('CLIENT') && showUserInfoWindow && <ClientInfo domain={domain} />}
        {jws.decode(localStorage.getItem('token')).payload.role.includes('PARKING_MANAGER') && showUserInfoWindow && <ClientInfo domain={domain} />}
        {jws.decode(localStorage.getItem('token')).payload.role.includes('PARKING_MANAGER') && <Statistics domain={domain} />}
        {showCreateParkingWindow && <CreateParking setShowCreateParkingWindow={setShowCreateParkingWindow} pData={pData} domain={domain} />}
        {showCreateParkingSpaceWindow && <CreateParkingSpace setShowCreateParkingSpaceWindow={setShowCreateParkingSpaceWindow} psData={psData} setParkingSpots={setParkingSpots} domain={domain} />}
        {showEditParkingSpaceWindow && <EditParkingSpace setShowEditParkingSpaceWindow={setShowEditParkingSpaceWindow} setParkingSpot={setParkingSpot} parkingSpot={parkingSpot} domain={domain} />}
        {showEditParkingWindow && <EditParking setShowEditParkingWindow={setShowEditParkingWindow} epData={epData} domain={domain} />}
        {showCalendarWindow && <Calendar setShowCalendarWindow={setShowCalendarWindow} calendarParkingId={calendarParkingId} domain={domain} />}
        {showParkingOccupationWindow && <ParkingSpotOccupation setParkingOccupationWindow={setParkingOccupationWindow} setParkingSpot={setParkingSpot} parkingSpot={parkingSpot} domain={domain} />}
    </div>);
}
//<UserInfo userData={userData}/>

export default Home;