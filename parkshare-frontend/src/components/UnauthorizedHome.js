import Login from "./Login";
import Register from "./Register";
import { useState } from 'react';

const UnauthorizedHome = ({ domain }) => {
    const [registerMsgToLogin, setMsg] = useState('');
    const token = localStorage.getItem('token');

    //check if user is already logged in
    console.log('Unauthorized');
    if (token) {
        window.location.replace('/home');
    }


    return (
        <div className="unauthorizedHome">
            <Login registerMsg={registerMsgToLogin} setMsg={setMsg} domain={domain} />
            <Register setMsg={setMsg} domain={domain} />
        </div>
    );
}

export default UnauthorizedHome;