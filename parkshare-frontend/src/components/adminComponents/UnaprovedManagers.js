import { useState, useEffect } from 'react';
import '../../styles/adminStyles/unaprovedList.css'

const UnaprovedManagers = ({ domain }) => {

    const [unaprovedList, setUnaproved] = useState(null);
    const [change, setChange] = useState(false);

    //runs everytime we render this component (it listenes when unaprovedList changes)
    useEffect(async function () {
        try {
            const response = await fetch(`${domain}api/admin/unapprovedmanagers`, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
            });

            if (response.status === 403) {
                localStorage.removeItem('token');
                window.location.replace('/');
            } else if (!response.ok) {
                throw Error('There was an error');
            }

            const data = await response.json();
            console.log(data);

            setUnaproved(data);

        } catch (err) {
            throw Error('response gone wrong');
        }
    }, [change]);

    const aproveManager = async function (username) {
        try {
            const response = await fetch(`${domain}api/admin/unapprovedmanagers/${username}`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
            });

            if (response.status === 403) {
                localStorage.removeItem('token');
                window.location.replace('/');
            } else if (!response.ok) {
                throw Error('There was an error');
            }

            change = setChange(!change);
        } catch (err) {
        }
    }

    return (
        <div className="popupDarkList" id="popupUnaprovedList">
            <div className="unaprovedList">
                {unaprovedList && unaprovedList.map((user) => (
                    <div className="listedUnaprovedUser" key={user.username}>
                        <div className="userData">
                            <div className="userStuff">
                                <div className="userUsername">{user.username}</div>
                                <div className="userName">{user.firstName}</div>
                                <div className="userLastname">{user.lastName}</div>
                            </div>
                        </div>
                        <div className="userButtons">
                            <button className="userButton" id="aprove" onClick={() => aproveManager(user.username)}>Aprove</button>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default UnaprovedManagers;