import $ from 'jquery';
import { useState, useEffect } from 'react';
import '../../styles/adminStyles/userInfo.css'

const UserInfo = ({ users, editUser, setEditUser, setChange, change, reload, domain }) => {

    let defaultInput = {
        username: "",
        role: "",
        firstName: "",
        lastName: "",
        email: "",
        image: ""
    };
    const [inputs, setInputs] = useState(defaultInput);
    const [message, setMessage] = useState('');


    //finds user from list of users
    let user = '';
    const reloadUser = () => {
        users.forEach(element => {
            if (element.username === editUser) {
                user = element;
                return;
            }
        });
    };
    reloadUser();

    function urltoFile(url, filename, mimeType) {
        return (fetch(url)
            .then(function (res) { return res.arrayBuffer(); })
            .then(function (buf) { return new File([buf], filename, { type: mimeType }); })
        );
    }

    //console.log(urltoFile({user.image}, 'id.png','image/png')
    //.then(function(file){ console.log(file);}));

    //fills data in text boxes
    useEffect(() => {
        console.log('reload')
        //reloadUser();
        setInputs(() => ({
            username: user.username,
            role: user.role,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            iban: user.iban,
            image: user.image
        }));
    }, [editUser, reload]);


    const handleChange = async (event) => {
        console.log('change');
        //load name atribute and value to inputs object
        const name = event.target.name;
        const value = event.target.value;

        if (value !== 'Change') {
            setInputs(values => ({
                ...values,
                [name]: value
            }));
        }
    }

    const handleSubmit = async (event) => {
        console.log('submit');
        setInputs(defaultInput);
        event.preventDefault();
        try {
            const image = document.getElementById("image");
            const imageValue = image.value;
            console.log(user.image)
            if (imageValue !== "") {
                inputs.image = image.files[0];
            }
            else {
                const file = await urltoFile(`${user.image}`, 'id.jpg', 'image/jpg');
                inputs.image = file;
            }

            const formData = new FormData();

            for (var key in inputs) {
                formData.append(key, inputs[key]);
            }

            const response = await fetch(`${domain}api/admin/users`, {       //change
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                body: formData
            });

            console.log("Sending data:");
            console.log(inputs);

            const jason = await response.text();

            if (response.ok) {
                setMessage('Changed user information');
            }
            else {
                setMessage("Error. " + jason);
            }

            setChange(!change);

        } catch (err) {
        }
    }

    var popUpManage = function (id1, id2) {
        const popup1 = $(id1);
        const popup2 = $(id2);

        if (popup1.css('display') === 'none' || popup1.css('display') === '') {
            popup1.show();
            popup2.hide();
        } else {
            popup1.hide();
        }

    }

    /**
     * Error: An invalid form control with name= is not focusable
     * baca se jer je input required, a ne moze ga provjeriti dok je hidden
     */
    return (
        <div id='popupEditWindow' className="userinfo" style={{ display: 'none' }}>
            <form className="userInfoForm" onSubmit={handleSubmit}>
                <h3>Edit profile {editUser}</h3>
                <label className="userInfoLabel" htmlFor="username">Username: </label>
                <input className="userInfoInput disabledInput" type="text" className="inputField" name="username" id="username" value={inputs.username ? inputs.username : ''} required minLength="4" disabled />
                <label className="userInfoLabel" htmlFor="role">Role: </label>
                <input className="userInfoInput disabledInput" type="text" className="inputField" name="role" id="role" value={inputs.role ? inputs.role : ''} required disabled />
                <label className="userInfoLabel" htmlFor="username">First name: </label>
                <input className="userInfoInput" type="text" className="inputField" name="firstName" id="name" value={inputs.firstName ? inputs.firstName : ''} required onChange={handleChange} />
                <label className="userInfoLabel" htmlFor="username">Last name: </label>
                <input className="userInfoInput" type="text" className="inputField" name="lastName" id="lastname" value={inputs.lastName ? inputs.lastName : ''} required onChange={handleChange} />
                <label className="userInfoLabel" htmlFor="username">Email: </label>
                <input className="userInfoInput disabledInput" type="email" className="inputField" name="email" id="email" value={inputs.email ? inputs.email : ''} required disabled />
                <label className="userInfoLabel" htmlFor="email">IBAN: </label>
                <input className="userInfoInput" type="text" className="inputField" name="iban" id="iban" value={inputs.iban ? inputs.iban : ''} required onChange={handleChange} />
                <input className="registerInput" type="file" className="imgField" name="image" id="image" accept="image/*" />
                <label className="registerLabel" htmlFor="image" className="custom-file-upload">
                    Upload new ID image
                </label>

                <input className="userInfoInput" type="submit" name="" id="submit" onClick={handleChange} value="Change" />
                <p>{message}</p>
                <button className="returnButton" onClick={() => { popUpManage('#userListPu', '#popupEditWindow'); setEditUser(''); }}>Return</button>
            </form>

        </div>
    );
}
//<img src={inputs.image}></img>
export default UserInfo;