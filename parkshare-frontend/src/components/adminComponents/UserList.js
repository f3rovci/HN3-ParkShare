import { useState, useEffect } from 'react';
import '../../styles/adminStyles/userlist.css';
import $ from 'jquery';
import UserInfo from "./UserInfo";
import jws from 'jws';

const UserList = ({ domain }) => {
    const [editUser, setEditUser] = useState('');
    const [change, setChange] = useState(false);
    const [reload, setReload] = useState(false);

    useEffect(() => {
        console.log('fetch')
        fetchUsers();
    }, [editUser, change]);


    const [users, setUsers] = useState([
        { username: "Hurrinade", role: "CLIENT", email: "test@gmail.com", name: "Marko", lastname: "Hello" },
        { username: "UraniumXI", role: "PARKING_MANAGER", email: "test1@gmail.com", name: "Marko", lastname: "Hello" },
        { username: "Marko", role: "CLIENT", email: "test5@gmail.com", name: "Marko", lastname: "Hello" },
        { username: "Ura", role: "PARKING_MANAGER", email: "test4@gmail.com", name: "Marko", lastname: "Hello" },
        { username: "UXI", role: "CLIENT", email: "test3@gmail.com", name: "Marko", lastname: "Hello" }]);


    var popUpManage = function (id1, id2) {
        const popup1 = $(id1);
        const popup2 = $(id2);

        if (popup1.css('display') === 'none' || popup1.css('display') === '') {
            popup1.show();
            popup2.hide();
        } else {
            popup1.hide();
        }

    }

    const fetchUsers = async () => {
        console.log('fetching')
        try {
            const response = await fetch(`${domain}api/admin/users`, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
            });
            const jason = await response.json();

            console.log('users:')
            console.log(jason);

            setUsers(jason);
            setReload(!reload);

        } catch (err) {

        }
    };

    const promoteToAdmin = async (u) => {
        try {
            const response = await fetch(`${domain}api/admin/users/${u}`, {
                method: 'POST',  //ask
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
            });
            const jason = await response.json();

            //console.log(jason);
        } catch (err) {

        }
    };

    return (
        <div className="popupDarkList" id="popupUserlist">
            <UserInfo users={users} editUser={editUser} setEditUser={setEditUser} setChange={setChange} change={change} reload={reload} />
            <div id='userListPu' className="userList">
                {users.map((user) => (
                    <div className="listedUser" key={user.username}>
                        <div className="userData">
                            <div className="userStuff">
                                <div className="userUsername">{user.username}</div>
                                <div className="userName">{user.firstName}</div>
                                <div className="userLastname">{user.lastName}</div>
                            </div>
                            <div className="userRole">{user.role}</div>
                        </div>
                        <div className="userButtons">
                            {(!(user.role === 'ADMIN')) && <button className="userButton" id="edit" onClick={() => { popUpManage('#popupEditWindow', '#userListPu'); setEditUser(user.username); }}>Edit</button>}
                            {(!(user.role === 'ADMIN')) && <button className="userButton" id="promote" onClick={() => { promoteToAdmin(user.username); setChange(!change); }}>Promote</button>}
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default UserList;