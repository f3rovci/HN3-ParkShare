import { useState } from "react";
import "../styles/login.css";
import $ from 'jquery';

const Login = ({ registerMsg, setMsg, domain }) => {
    const initialState = {
        password: '',
        username: ''
    }


    const [inputs, setInputs] = useState(initialState);
    const [msg, setMsgUS] = useState();
    let infoMsg;

    if (registerMsg !== '') {
        infoMsg = registerMsg;
        if (infoMsg === null)
            infoMsg = '';
        if (msg !== infoMsg)
            setMsgUS(infoMsg);
        setMsg('');
    }




    const setInfoMsgWindow = () => {
        document.getElementById('msgWindow').classList.add('infoMsgWindow');
        document.getElementById('msgWindow').classList.remove('errorMsgWindow');
    }

    const setErrorMsgWindow = () => {
        document.getElementById('msgWindow').classList.remove('infoMsgWindow');
        document.getElementById('msgWindow').classList.add('errorMsgWindow');
    }

    const handleChange = (event) => {
        //load name atribute and value to inputs object
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log("Sending data:");
        console.log(JSON.stringify(inputs));
        //console.log(domain)

        try {
            //f3rovci-parkshare.herokuapp.com
            const response = await fetch(`${domain}api/login?username=${inputs.username}&password=${inputs.password}`, {
                method: 'POST'
            });



            //console.log('login');
            //console.log(jason);

            if (!response.ok) {
                setErrorMsgWindow();
                setMsgUS('Wrong email or password');
            }
            else {
                const jason = await response.json();
                if (jason.boolean === 'true') {
                    localStorage.setItem('token', jason.access_token);
                    localStorage.setItem('username', inputs.username);
                    //console.log('New token received: ' + localStorage.getItem('token'));
                    setMsg('');
                    setMsgUS('');
                    setInfoMsgWindow();
                    window.location.replace("/home");
                }
                else {
                    setInfoMsgWindow();

                    if (jason.Role === 'UNAPPROVED') {
                        setMsg("Administrator didn't approve you yet");
                    }
                    else {
                        setMsg('Verify your email address before you can login');
                    }
                }
            }

        } catch (err) {
            setErrorMsgWindow();
            setMsgUS('Error occurred');
        }
    }

    $(document).on("keydown", function (e) {
        if (e.keyCode === 27) {
            $('#popupLogin').hide();
        }
    });

    return (
        <div className="popupDark" id="popupLogin">
            <div id='login' className='login'>
                <h1 className="title">Login</h1>
                <div className="formContainer">
                    <form className="loginForm" onSubmit={handleSubmit}>
                        <label className="loginLabel" htmlFor="username">Username :</label>
                        <input className="loginInput" type="text" id="username" name="username" value={inputs.username} onChange={handleChange} required minLength="4" /> <br />
                        <label className="loginLabel" htmlFor="password">Password :</label>
                        <input className="loginInput" type="password" id="password" name="password" value={inputs.password} onChange={handleChange} required minLength="6" /> <br />
                        <p id="msgWindow" className="infoMsgWindow">{msg}</p><br />
                        <input id="loginInputButton" className="loginInput" type="submit" value="Login" />
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Login;
