import { useEffect, useState } from "react";
//import CreateParking from './managerComponents/CreateParking'
import L from 'leaflet';
import jws from 'jws';
import '../styles/manager.css'
import $ from 'jquery';
//import '../styles/map.css';

require('leaflet-routing-machine');
require('leaflet-editable');
require('leaflet-path-transform');


function Map({ setParkingSpot, domain, setParkingOccupationWindow, setShowCreateParkingWindow, setPolygonData, setShowEditParkingSpaceWindow, setShowEditParkingWindow, setShowCreateParkingSpaceWindow, setParkingSpaceData, allParkingSpots, setShowCalendarWindow, setCalendarParkingId }) {

    let isDrawingPolygon = false;
    let isCreatingParkingSpace = false;
    let isFindingClosest = false;
    let isUsingCar = true;

    let polygon;
    let map;
    let routing

    let parkingId;

    let trigger = false;



    let parkingSpacesMap = [];

    let openEditParkingSpaceWindow = function () {
        setShowEditParkingSpaceWindow(true);
    }

    //Create parking space window
    let openCreateParkingSpaceWindow = function (parking) {
        const parkingSpaceData = {
            long1: parking._latlngs[0][0].lng,
            long2: parking._latlngs[0][1].lng,
            long3: parking._latlngs[0][2].lng,
            long4: parking._latlngs[0][3].lng,
            lat1: parking._latlngs[0][0].lat,
            lat2: parking._latlngs[0][1].lat,
            lat3: parking._latlngs[0][2].lat,
            lat4: parking._latlngs[0][3].lat,
            parkingLotId: parkingId
        };

        setParkingSpaceData(parkingSpaceData);
        setShowCreateParkingSpaceWindow(true);
    }

    let openParkingWindow = function () {
        setShowEditParkingWindow(true);
    }

    //Create parking window
    let openParkingRegistration = function () {

        if (polygon && polygon._parts[0].length === 4) {
            $('#drawButton').hide();
            $('#publishButton').hide();


            const polygonData = {
                long1: polygon._latlngs[0][0].lng,
                long2: polygon._latlngs[0][1].lng,
                long3: polygon._latlngs[0][2].lng,
                long4: polygon._latlngs[0][3].lng,
                lat1: polygon._latlngs[0][0].lat,
                lat2: polygon._latlngs[0][1].lat,
                lat3: polygon._latlngs[0][2].lat,
                lat4: polygon._latlngs[0][3].lat,
            };

            setPolygonData(polygonData);

            setShowCreateParkingWindow(true);
            //setPolygonData(polygonData);
        } else if (polygon) {
            alert(`You only have ${polygon._parts[0].length} points, you need 4 points to create parking`);
        } else {
            alert('You did not draw anything');
        }
    }

    var updateParkingSpaces = async function (ps, id) {
        try {

            const formData = new FormData();

            formData.append('longitude1', ps._latlngs[0][0].lng);
            formData.append('longitude2', ps._latlngs[0][1].lng);
            formData.append('longitude3', ps._latlngs[0][2].lng);
            formData.append('longitude4', ps._latlngs[0][3].lng);
            formData.append('latitude1', ps._latlngs[0][0].lat);
            formData.append('latitude2', ps._latlngs[0][1].lat);
            formData.append('latitude3', ps._latlngs[0][2].lat);
            formData.append('latitude4', ps._latlngs[0][3].lat);
            formData.append('id', id);

            const response = await fetch(`${domain}api/manager/ps-edit-coordinates`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                body: formData
            });

            ////console.log(response);

            let res = await response.text();
            //console.log(res)

        } catch (e) {
            //console.log(e);
        }
    }

    let fetchOccupyBikeSpace = async function (occupy, role) {
        try {
            const formData = new FormData();

            formData.append('parkingLotId', localStorage.getItem('parkingLotID'));
            formData.append('occupy', occupy);

            const response = await fetch(`${domain}api/${role}/manage-bike-space`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                body: formData
            });

            let r = response.text();
            r.then((e) => {
                if (role === 'manager')
                    alert(e);
                return false;
            })
        } catch (err) {
            console.log(err);
            return false;
        }

        return true;
    }

    //FETCH PARKING LOTS FOR NON REGISTERED PERSON
    let fetchNonRegisteredParkingLots = async function () {
        try {
            const response = await fetch(`${domain}api/view-all-parking-lots`, {
                method: 'GET'
            });

            //console.log(response);

            if (!response.ok) {
                throw Error('Could not fetch data');
            } else {
                let s = await response.json();
                //console.log(s)

                for (let i = 0; i < s.length; i++) {
                    var pts = [
                        [s[i].latitude1, s[i].longitude1],
                        [s[i].latitude2, s[i].longitude2],
                        [s[i].latitude3, s[i].longitude3],
                        [s[i].latitude4, s[i].longitude4]
                    ];

                    polygon = L.polygon(pts).addTo(map);

                    fetchNonRegisteredParkingSpaces();
                }


            }



        } catch (e) {
            //console.log(e);
        }
    }

    let fetchNonRegisteredParkingSpaces = async function () {
        try {
            const response = await fetch(`${domain}api/view-all-parking-spaces`, {
                method: 'GET'
            });

            //console.log(response);

            if (!response.ok) {

                throw Error('Could not fetch data');

            } else {
                let parkingSpacesData = await response.json();

                //console.log(parkingSpacesData);

                parkingSpacesData.forEach(element => {
                    var pts = [
                        [element.latitude1, element.longitude1],
                        [element.latitude2, element.longitude2],
                        [element.latitude3, element.longitude3],
                        [element.latitude4, element.longitude4]
                    ];

                    let parkingSpot = L.polygon(pts).addTo(map);
                })
            }



        } catch (e) {
            //console.log(e);
        }
    }

    //FETCH SAVED PARKING LOT FROM SERVER
    let fetchParkingLot = async function (username) {
        try {
            const response = await fetch(`${domain}api/manager/show-parking-lots-manager/${username}`, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
            });

            if (response.ok) {
                $('#parking-exists').show();
                $('#parking-not-exists').hide();
                let parkingLotData = await response.json();

                ////console.log(parkingLotData)

                localStorage.setItem('parkingLotID', parkingLotData.id);

                parkingId = parkingLotData.id;
                polygon = L.polygon([
                    [parkingLotData.latitude1, parkingLotData.longitude1],
                    [parkingLotData.latitude2, parkingLotData.longitude2],
                    [parkingLotData.latitude3, parkingLotData.longitude3],
                    [parkingLotData.latitude4, parkingLotData.longitude4]
                ]).addTo(map);

                let parkingLotCenterLat = (Number(parkingLotData.latitude1) + Number(parkingLotData.latitude2) + Number(parkingLotData.latitude3) + Number(parkingLotData.latitude4)) / 4;
                let parkingLotCenterLng = (Number(parkingLotData.longitude1) + Number(parkingLotData.longitude2) + Number(parkingLotData.longitude3) + Number(parkingLotData.longitude4)) / 4

                map.setView(new L.LatLng(
                    parkingLotCenterLat,
                    parkingLotCenterLng
                ), 17);

                try {
                    const response = await fetch(`${domain}api/manager/show-parking-spaces-manager/${parkingId}`, {
                        method: 'GET',
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.getItem('token')
                        }
                    });

                    ////console.log(response);

                    if (!response.ok) {
                        throw Error('Could not fetch the data');
                    }
                    let parkingSpaces = await response.json();
                    console.log(parkingSpaces)

                    for (let parkingSpace in parkingSpaces) {
                        ////console.log(parkingSpace)
                        var pts = [
                            [parkingSpaces[parkingSpace].latitude1, parkingSpaces[parkingSpace].longitude1],
                            [parkingSpaces[parkingSpace].latitude2, parkingSpaces[parkingSpace].longitude2],
                            [parkingSpaces[parkingSpace].latitude3, parkingSpaces[parkingSpace].longitude3],
                            [parkingSpaces[parkingSpace].latitude4, parkingSpaces[parkingSpace].longitude4]
                        ];

                        let parking = L.polygon(pts, { transform: true, draggable: true }).on('dragstart', () => {
                            parking.positionBeforeDragStart = parking.getLatLngs();
                        }).on('dragend', (e) => {
                            if (!isInsidePolygon(calculateCenterPoint(e.target._latlngs[0]), polygon)) {
                                parking.setLatLngs(parking.positionBeforeDragStart);
                                parking.transform.disable();
                                parking.dragging.disable();
                                parking.transform.enable();
                                parking.dragging.enable();
                            }
                            //console.log(e)
                        }).on('click', () => {
                            if (document.getElementById('editPS').classList.contains('buttonActive')) {
                                //console.log(parking)
                                ////console.log(psEditData)
                                setParkingSpot(parking);

                                openEditParkingSpaceWindow();
                                $('#parking-exists').hide();
                            } else {
                                setParkingSpot(parking);

                                setParkingOccupationWindow(true);
                            }

                        }).addTo(map);

                        parking.transform.disable();
                        parking.dragging.disable();

                        parking.positionBeforeDragStart = parking.getLatLngs();
                        parking.parkingSpaceId = parkingSpaces[parkingSpace].id;
                        parking.bookable = parkingSpaces[parkingSpace].bookable;
                        parking.available = parkingSpaces[parkingSpace].available;

                        if (parking.available) {
                            parking.setStyle({
                                color: 'green'
                            });
                        }
                        else {
                            parking.setStyle({
                                color: 'red'
                            });
                        }

                        parkingSpacesMap.push(parking);
                    }

                } catch (e) {
                    //console.log(e);
                }



            }
            else {
                $('#parking-exists').hide();
                $('#parking-not-exists').show();
            }

            map.invalidateSize()
        } catch (e) {
            //console.log(e);
        }
    }

    let fetchParkingLots = async function () {
        try {
            const response = await fetch(`${domain}api/client/clientview-parking-lots/`, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
            });


            if (!response.ok) {
                throw Error('Could not fetch data');
            } else {
                let s = await response.json();

                for (let i = 0; i < s.length; i++) {
                    var pts = [
                        [s[i].latitude1, s[i].longitude1],
                        [s[i].latitude2, s[i].longitude2],
                        [s[i].latitude3, s[i].longitude3],
                        [s[i].latitude4, s[i].longitude4]
                    ];

                    polygon = L.polygon(pts).addTo(map);
                }

                fetchParkingSpaces();
            }

        } catch (e) {
            //console.log(e);
        }

    }

    let clientParkingSpaces = [];
    let fetchParkingSpaces = async function () {
        try {

            const response = await fetch(`${domain}api/client/view-availble-parking-spaces`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
            });

            //console.log(response)

            let s = await response.json();
            console.log(s)


            s.forEach(element => {
                var pts = [
                    [parseFloat(element.latitude1), parseFloat(element.longitude1)],
                    [parseFloat(element.latitude2), parseFloat(element.longitude2)],
                    [parseFloat(element.latitude3), parseFloat(element.longitude3)],
                    [parseFloat(element.latitude4), parseFloat(element.longitude4)]
                ];

                ////console.log(pts)

                let parkingSpot = L.polygon(pts).on('click', () => {
                    //console.log('clicked')
                    ////console.log(parkingSpot.parkingSpaceId)
                    //                                                                                              change
                    /*
                    if (parkingSpot.bookable && parkingSpot.available) {
                        setCalendarParkingId(parkingSpot.parkingSpaceId);
                        setShowCalendarWindow(true);
                    } else {
                        alert('This parking can not be reserved');
                    }
                    */

                    if (parkingSpot.occupiedByManager) {
                        alert('Can not reserve this parking space');
                    } else if (parkingSpot.bookable) {
                        setCalendarParkingId(parkingSpot.parkingSpaceId);
                        setShowCalendarWindow(true);
                    } else {
                        alert('This parking can not be reserved');
                    }



                    $('#button-container-holder').hide();

                    //$('#parking-exists').hide();
                }).addTo(map);

                console.log(parkingSpot)

                if (element.available) {
                    parkingSpot.setStyle({
                        color: 'green'
                    });
                }
                else {
                    parkingSpot.setStyle({
                        color: 'red'
                    });
                }

                parkingSpot.parkingSpaceId = element.id;
                parkingSpot.bookable = element.bookable;
                parkingSpot.available = element.available;
                parkingSpot.occupiedByManager = element.occupiedByManager;


                let centLat = 0;
                let centLng = 0;
                pts.forEach(e => {
                    centLat += e[0];
                    centLng += e[1];
                })
                centLat /= pts.length;
                centLng /= pts.length;

                parkingSpot.center = L.latLng(centLat, centLng);

                clientParkingSpaces.push(parkingSpot);

            })

            //console.log(clientParkingSpaces)
        }
        catch (e) {
            //console.log(e);
        }
    }

    //here
    let fetchBikeSpaces = async function (coord) {
        let is;
        try {
            const formData = new FormData();
            formData.append('longitude', coord[0]);
            formData.append('latitude', coord[1]);

            //link
            const response = await fetch(`${domain}api/client/getBikeParking`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                body: formData
            });

            is = await response.json()
            console.log(is)
            if (!is.found) {
                return is.error;
            }


            //clientParkingSpaces = [];
            //clientParkingSpaces.push([is.latitude, is.longitude])

            //L.marker([is.latitude, is.longitude]).addTo(map)
        }
        catch (e) {
            console.log(e)
        }
        return L.latLng(is.latitude, is.longitude);
    }

    //sets up the map
    useEffect(() => {





        console.log("Map refresh")

        for (let spot in allParkingSpots) {
            //console.log(allParkingSpots[spot]);
        }

        let current_lat = 45.815399;
        let current_long = 15.966568;
        let current_zoom = 16;
        let center_lat = current_lat;
        let center_long = current_long;
        let center_zoom = current_zoom;


        /*fixes Map container is already initialized error
        var container = L.DomUtil.get('map');
        if(container !== null){
            container._leaflet_id = null;
        }
        */

        // The <div id="map"> must be added to the dom before calling L.map('map')
        map = L.map('map', {
            //drawControl: true,
            zoomControl: false,
            center: [center_lat, center_long],
            zoom: center_zoom,
            editable: true
        });


        L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
            attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            maxZoom: 22,
            maxNativeZoom: 19
        }).addTo(map);


        L.Control.Watermark = L.Control.extend({

            //if the user is parking manager returns a container with buttons, otherwise returns an empty container
            onAdd: function (map) {
                if (jws.decode(localStorage.getItem('token')) && jws.decode(localStorage.getItem('token')).payload.role.includes('PARKING_MANAGER')) {
                    fetchParkingLot(localStorage.getItem('username'));
                    var container = L.DomUtil.create('div');
                    container.id = 'button-container-holder';

                    //#region buttons

                    var editBtn = L.DomUtil.create('button');
                    editBtn.textContent = 'Edit parking';
                    editBtn.id = 'editButton';
                    editBtn.className = 'mapButton';
                    editBtn.onclick = () => {
                        openParkingWindow();
                        $('#parking-exists').hide();
                    };


                    //DONT DELETE
                    var createPS = L.DomUtil.create('button');
                    createPS.textContent = 'Add parking spot';
                    createPS.id = 'createPS';
                    createPS.className = 'mapButton';
                    createPS.onclick = () => {
                        isCreatingParkingSpace = !isCreatingParkingSpace;
                        $('#bike-parking').hide();

                        if (isCreatingParkingSpace) {
                            $('#createPS').addClass('buttonActive');
                            parkingSpacesMap.forEach(element => {
                                element.transform.disable();
                                element.dragging.disable();
                            });
                            createPS.textContent = 'Confirm';
                            $('#editPS').hide();
                            $('#editButton').hide();

                        }
                        else {
                            $('#createPS').removeClass('buttonActive');
                            window.location.reload();
                        }
                    };

                    //DONT DELETE
                    var editPS = L.DomUtil.create('button');
                    editPS.textContent = 'Edit parking spot';
                    editPS.id = 'editPS';
                    editPS.className = 'mapButton';
                    editPS.onclick = () => {

                        if (document.getElementById('editPS').classList.contains('buttonActive')) {
                            editPS.textContent = 'Edit parking spot';
                            $('#bike-parking').show();

                            if (parkingSpacesMap.length > 0) {
                                parkingSpacesMap.forEach(element => {
                                    element.transform.disable();
                                    element.dragging.disable();
                                });

                                for (let i = 0; i < parkingSpacesMap.length; i++) {
                                    updateParkingSpaces(parkingSpacesMap[i], parkingSpacesMap[i].parkingSpaceId);
                                }
                            }

                            $('#createPS').show();
                            $('#editButton').show();
                            $('#editPS').removeClass('buttonActive');
                        }
                        else {
                            editPS.textContent = 'Confirm edit';
                            $('#bike-parking').hide();

                            $('#createPS').hide();
                            $('#editButton').hide();
                            parkingSpacesMap.forEach(element => {
                                element.transform.disable();
                                element.dragging.disable();
                                element.transform.enable();
                                element.dragging.enable();
                            });

                            isCreatingParkingSpace = false;
                            $('#editPS').addClass('buttonActive');
                        }

                        // parkingSpacesMap.forEach(element => { console.log(element) });
                    };
                    //#endregion

                    var buttonContainer1 = L.DomUtil.create('div');
                    buttonContainer1.id = 'parking-exists';
                    buttonContainer1.appendChild(editBtn);
                    buttonContainer1.appendChild(createPS);
                    buttonContainer1.appendChild(editPS);


                    //mogucnost kreiranja parkinga
                    var drawBtn = L.DomUtil.create('button');
                    drawBtn.textContent = 'Draw';
                    drawBtn.id = 'drawButton';
                    drawBtn.className = 'mapButton';
                    drawBtn.onclick = () => {
                        isDrawingPolygon = !isDrawingPolygon;

                        if (isDrawingPolygon === true) {
                            drawBtn.textContent = 'Drawing';
                            $('#drawButton').addClass('buttonActive');
                        }
                        else {
                            drawBtn.textContent = 'Draw';
                            $('#drawButton').removeClass('buttonActive');
                        };
                    }

                    var publishBtn = L.DomUtil.create('button');
                    publishBtn.textContent = 'Publish parking';
                    publishBtn.id = 'publishButton';
                    publishBtn.className = 'mapButton';
                    publishBtn.onclick = () => {
                        openParkingRegistration();
                    }

                    var buttonContainer2 = L.DomUtil.create('div');
                    buttonContainer2.id = 'parking-not-exists';
                    buttonContainer2.appendChild(drawBtn);
                    buttonContainer2.appendChild(publishBtn);



                    var occupyBikeSpace = L.DomUtil.create('button');
                    occupyBikeSpace.textContent = 'Occupy bike space';
                    occupyBikeSpace.id = 'occupyBikeSpaceButton';
                    occupyBikeSpace.className = 'mapButton';
                    occupyBikeSpace.onclick = () => { fetchOccupyBikeSpace(true, 'manager') };

                    var freeBikeSpace = L.DomUtil.create('button');
                    freeBikeSpace.textContent = 'Free bike space';
                    freeBikeSpace.id = 'freeBikeSpaceButton';
                    freeBikeSpace.className = 'mapButton';
                    freeBikeSpace.onclick = () => { fetchOccupyBikeSpace(false, 'manager') };

                    var buttonContainer3 = L.DomUtil.create('div');
                    buttonContainer3.id = 'bike-parking';
                    buttonContainer3.appendChild(occupyBikeSpace);
                    buttonContainer3.appendChild(freeBikeSpace);


                    buttonContainer1.appendChild(buttonContainer3);
                    container.appendChild(buttonContainer1);
                    container.appendChild(buttonContainer2)


                    //without this markers are placed on the map when clicking the button
                    L.DomEvent.on(
                        container,
                        'click',
                        (e) => {
                            e.stopPropagation();
                        }
                    );

                    return container;
                }
                else if (jws.decode(localStorage.getItem('token')) && jws.decode(localStorage.getItem('token')).payload.role.includes('CLIENT')) {

                    //fetchParkingLots();
                    //fetchParkingSpaces();

                    var findClosestBtn = L.DomUtil.create('button');
                    findClosestBtn.textContent = 'Find closest parking';
                    findClosestBtn.id = 'findClosestButton';
                    findClosestBtn.className = 'mapButton';
                    findClosestBtn.onclick = () => {
                        if ($('#findClosestButton').hasClass('buttonActive')) {
                            $('#findClosestButton').removeClass('buttonActive');
                            findClosestBtn.textContent = 'Find closest parking';
                            isFindingClosest = false;
                        }
                        else {
                            if (routing !== undefined) {
                                map.removeControl(routing);
                                routing = undefined;
                            }
                            $('#findClosestButton').addClass('buttonActive');
                            findClosestBtn.textContent = 'Mark your destination';
                            isFindingClosest = true;
                        }
                    };
                    var toggleVehicleBtn = L.DomUtil.create('button');
                    toggleVehicleBtn.textContent = 'Using car';
                    toggleVehicleBtn.id = 'toggleVehicle';
                    toggleVehicleBtn.className = 'mapButton';
                    toggleVehicleBtn.onclick = () => {
                        $('#findClosestButton').text('Find closest parking')
                        $('#findClosestButton').removeClass('buttonActive');
                        isFindingClosest = false;

                        if (routing !== undefined) {
                            map.removeControl(routing);
                            routing = undefined;
                        }

                        if (isUsingCar) {
                            toggleVehicleBtn.textContent = 'Using bike';
                            isUsingCar = false;
                            polygon = undefined;
                            clientParkingSpaces = [];
                        }
                        else {
                            toggleVehicleBtn.textContent = 'Using car';
                            isUsingCar = true;
                            //fetchParkingLots();
                        }
                        //heree
                        $('#toggleShowParkingsButton').removeClass('buttonActive');
                        $('#toggleShowParkingsButton').text('Show all parkings');
                        map.eachLayer(function (layer) {
                            if (!!layer.toGeoJSON) {
                                map.removeLayer(layer);
                            }
                        });
                    };

                    var toggleShowParkingsBtn = L.DomUtil.create('button');
                    toggleShowParkingsBtn.textContent = 'Show all parkings';
                    toggleShowParkingsBtn.id = 'toggleShowParkingsButton';
                    toggleShowParkingsBtn.className = 'mapButton';
                    toggleShowParkingsBtn.onclick = () => {
                        if (routing !== undefined) {
                            map.removeControl(routing);
                            routing = undefined;
                        }

                        if ($('#toggleShowParkingsButton').hasClass('buttonActive')) {
                            $('#toggleShowParkingsButton').removeClass('buttonActive');
                            toggleShowParkingsBtn.textContent = 'Show all parkings';
                            map.eachLayer(function (layer) {
                                if (!!layer.toGeoJSON) {
                                    map.removeLayer(layer);
                                }
                            });
                        }
                        else {
                            fetchParkingLots();

                            $('#toggleShowParkingsButton').addClass('buttonActive');
                            toggleShowParkingsBtn.textContent = 'Hide all parkings';
                        }
                    }

                    var container = L.DomUtil.create('div');
                    container.id = 'button-container-holder';
                    container.appendChild(findClosestBtn);
                    container.appendChild(toggleVehicleBtn);
                    container.appendChild(toggleShowParkingsBtn);


                    L.DomEvent.on(
                        container,
                        'click',
                        (e) => {
                            e.stopPropagation();
                        }
                    );
                    return container;
                } else {
                    if (true) {
                        console.log(map.getZoom());

                    }
                    fetchNonRegisteredParkingLots();
                    return L.DomUtil.create('div');
                }
            },

            onRemove: function (map) {
                L.DomEvent.off();
            }
        });

        L.control.watermark = function (opts) {
            return new L.Control.Watermark(opts);
        }


        L.control.watermark({ position: 'topright' }).addTo(map);
        $('#parking-exists').hide();
        $('#parking-not-exists').hide();

        var offs = [0.00001057048, 0.00001202914];


        let pointsForRouting = [];
        let setPointsForRouting = function () {
            clientParkingSpaces.forEach(element => {
                if (element.center === undefined) {
                    pointsForRouting.push([element.latitude, element.longitude]);
                }
                else
                    pointsForRouting.push([element.center.lat, element.center.lng]);
            });
        }

        let distances = [];
        let fetchShortestPath = async function (coords) {
            let distance = 0;
            let response;
            try {
                //
                response = await fetch(`http://router.project-osrm.org/route/v1/walking/${coords}?overview=false`, {
                    method: 'GET',
                });



                const data = await response.json();
                distance = data.routes[0].distance

                distances.push(distance);

            }
            catch (err) {
                //console.log(err)
            }
            return response
        }

        let data;
        let fetchShortestPath2 = async function (coord) {
            data = undefined;
            let response;
            const formData = new FormData();
            formData.append('longitude', coord[0]);
            formData.append('latitude', coord[1]);

            try {
                response = await fetch(`${domain}api/client/getCarParking`, {
                    method: 'POST',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('token')
                    },
                    body: formData
                });
                data = await response.json();
                console.log(data)

                if (response.status === 409) {
                    alert(data)
                }
            }
            catch (err) {
                //console.log(err)
            }
            return response
        }

        let fetchReserveParkingSpace = async function () {
            try {
                const formData = new FormData();
                //formData.append('startTime', new Date().getTime());
                //formData.append('endTime', new Date().getTime() + 2 * 60 * 60 * 1000);
                formData.append('id', data.id);
                formData.append('username', localStorage.getItem('username'));

                let response = await fetch(`${domain}api/parking/reservationDefault`, {
                    method: 'POST',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('token')
                    },
                    body: formData
                });

                if (!response.ok) {
                    alert('Not enaugh money for reservation');
                } else {
                    data = await response.json();
                }

            }
            catch (err) {
                console.log(err)
            }
        }


        let bikeParking;
        let destinationPoint;
        //map events
        map.on('click', onMapClick);
        function onMapClick(e) {
            if (isFindingClosest) {

                if (routing !== undefined) {
                    map.removeControl(routing);
                    routing = undefined;
                }

                if (isUsingCar) {
                    pointsForRouting = [];
                    //fetchParkingSpaces();
                    setPointsForRouting();
                    distances = [];



                    if (document.getElementById('findClosestButton').textContent === 'Mark your starting point') {
                        let startingPoint = [e.latlng.lat, e.latlng.lng];
                        isFindingClosest = false;
                        $('#findClosestButton').removeClass('buttonActive');
                        document.getElementById('findClosestButton').textContent = 'Find closest parking'
                        console.log(destinationPoint)

                        let res = fetchShortestPath2([destinationPoint[0], destinationPoint[1]]);

                        res.then(() => {
                            if (data !== undefined) {
                                if (data.bookable) {
                                    fetchReserveParkingSpace();
                                }

                                let pts = [
                                    L.latLng(data.latitude1, data.longitude1),
                                    L.latLng(data.latitude2, data.longitude2),
                                    L.latLng(data.latitude3, data.longitude3),
                                    L.latLng(data.latitude4, data.longitude4)
                                ];

                                let center = calculateCenterPoint(pts)

                                L.polygon(pts).addTo(map);

                                routing = L.Routing.control({
                                    waypoints: [
                                        L.latLng(startingPoint[0], startingPoint[1]),
                                        L.latLng(center.lat, center.lng)
                                    ]
                                }).addTo(map);
                            }
                            else {
                                alert('No available parking spaces');
                            }

                        })
                    }
                    else {
                        destinationPoint = [e.latlng.lat, e.latlng.lng];
                        document.getElementById('findClosestButton').textContent = 'Mark your starting point';
                    }

                }
                else {
                    if (document.getElementById('findClosestButton').textContent === 'Mark your starting point') {
                        console.log('second')
                        isFindingClosest = false;
                        $('#findClosestButton').removeClass('buttonActive');
                        document.getElementById('findClosestButton').textContent = 'Find closest parking'

                        pointsForRouting = [];
                        setPointsForRouting();
                        let result = fetchOccupyBikeSpace(true, 'client');

                        if (result) {
                            routing = L.Routing.control({
                                waypoints: [
                                    L.latLng(e.latlng.lat, e.latlng.lng),
                                    L.latLng(bikeParking.lat, bikeParking.lng)
                                ]
                            }).addTo(map);
                        }
                    }
                    else {
                        console.log('first')
                        document.getElementById('findClosestButton').textContent = 'Mark your starting point';
                        let prms = fetchBikeSpaces([e.latlng.lat, e.latlng.lng]);
                        prms.then((e) => {
                            if (e === undefined) {
                                alert('There are no free bike parking spaces')
                                isFindingClosest = false;
                                $('#findClosestButton').removeClass('buttonActive');
                                document.getElementById('findClosestButton').textContent = 'Find closest parking'
                            }
                            else {
                                bikeParking = e
                            }
                        })
                    }
                }

            }

            if (isCreatingParkingSpace && isInsidePolygon(e.latlng, polygon)) {

                //  because parking space is small will throw error if you try to make one while zoomed out really far
                var pts = [
                    [e.latlng.lat + offs[0], e.latlng.lng + offs[1]],
                    [e.latlng.lat + offs[0], e.latlng.lng - offs[1]],
                    [e.latlng.lat - offs[0], e.latlng.lng - offs[1]],
                    [e.latlng.lat - offs[0], e.latlng.lng + offs[1]],
                ];

                var parking = L.polygon(pts, { transform: true, draggable: true }).on('dragend', (e) => {
                    if (!isInsidePolygon(calculateCenterPoint(e.target._latlngs[0]), polygon))
                        L.popup().setLatLng(calculateCenterPoint(e.target._latlngs[0])).setContent("You wont be able to save until you move the parking space inside parking lot").openOn(map);
                }).addTo(map);

                openCreateParkingSpaceWindow(parking);

                parking.transform.disable();
                parking.dragging.disable();
                map.fitBounds(pts);
            }
            else if (isCreatingParkingSpace) {
                L.popup().setLatLng(e.latlng).setContent("Cant create parking space outside parking lot").openOn(map);
            }

            if (isDrawingPolygon) {
                if (polygon === undefined) {
                    polygon = L.polygon([
                        [e.latlng.lat, e.latlng.lng]
                    ]).addTo(map);
                }
                else {
                    let points = polygon._latlngs[0];

                    if (points.length < 4) {
                        points.push(e.latlng);
                    } else {
                        alert('Maximum points reached (4) you can not add more')
                    }


                    polygon.remove();



                    //polygon = L.polygon(points).addTo(map).on('click', () => { polygon.remove(); polygon = undefined; });
                    polygon = L.polygon(points).addTo(map);
                }
            }

            /* primjer za routing
            if(startMarker !== undefined && endMarker !== undefined){
                L.Routing.control({
                    waypoints: [
                      L.latLng(startMarker._latlng.lat, startMarker._latlng.lng),
                      L.latLng(endMarker._latlng.lat, endMarker._latlng.lng)
                    ]
                  }).addTo(map);
         
                startMarker.remove();
                startMarker = undefined;
                endMarker.remove();
                endMarker = undefined;
            }
             */

        }

    }, [trigger]);


    //used to check if parking space is inside parking lot
    function isInsidePolygon(latlng, polygon) {
        if (polygon === undefined) return false;

        var polyPoints = polygon.getLatLngs()[0];
        var x = latlng.lat, y = latlng.lng;

        ////console.log(polygon.getLatLngs())

        var inside = false;
        for (var i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
            var xi = polyPoints[i].lat, yi = polyPoints[i].lng;
            var xj = polyPoints[j].lat, yj = polyPoints[j].lng;

            var intersect = ((yi > y) != (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }

        return inside;
    };

    function calculateCenterPoint(latlngs) {
        if (latlngs === undefined) return null;

        let l = latlngs.length;
        let lat = 0;
        let lng = 0;

        latlngs.forEach(latlng => {
            lat += latlng.lat;
            lng += latlng.lng;
        });

        return L.latLng(lat / l, lng / l);
    }

    return (
        <div className="Map" id='map'>
            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
                integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
                crossOrigin="" />
            <link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" />
            <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
                integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
                crossOrigin=""></script>
            <script src="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.js"></script>

        </div>
    );
}

export default Map;

/*
import { MapContainer, TileLayer, LayersControl, Popup } from 'react-leaflet';
import jws from 'jws';
import ManagerButtons from './managerComponents/ManagerButtons';
import CreateParking from './managerComponents/CreateParking';

const Map = () => {

    return (
        <div className="map" id='map'>
            <MapContainer center={[45.8150, 15.9819]} zoom={13} scrollWheelZoom={true} preferCanvas={true}>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    maxNativeZoom={18}
                    maxZoom={21}
                />

                {jws.decode(localStorage.getItem('token')) && jws.decode(localStorage.getItem('token')).payload.role.includes('PARKING_MANAGER') && <ManagerButtons position="topright" />}
                {jws.decode(localStorage.getItem('token')) && jws.decode(localStorage.getItem('token')).payload.role.includes('PARKING_MANAGER') && <CreateParking />}

            </MapContainer>
            <script src="https://cdn.jsdelivr.net/npm/leaflet.path.drag@0.0.6/src/Path.Drag.min.js"></script>
        </div>
    );
}

export default Map;
*/


//                                                                  WORKING ROUTING DONT DELETE

//goes above map.on('click', onMapClick);
/*
let pointsForRoutingTest =
            [
                [45.80917902561325, 15.977210998535158],
                [45.806247361660056, 16.00218772888184],
                [45.798349050263155, 15.935754776000978],

                [45.82114340079471, 15.95867156982422],
                [45.81187123414431, 15.994806289672853],
                [45.81109349837976, 15.937814712524414],
                [45.801699985789696, 16.012573242187504],
                [45.79146703293602, 15.979957580566408],
                [45.78793592896303, 15.945281982421877]

            ]
        pointsForRoutingTest.forEach(element => {
            L.marker(L.latLng(element[0], element[1])).addTo(map);
        });

        let distances = [];
        let fetchShortestPath = async function (coords) {
            let distance = 0;
            let response;
            try {
                //
                response = await fetch(`http://router.project-osrm.org/route/v1/driving/${coords}?overview=false`, {
                    method: 'GET',
                });

                const data = await response.json();
                distance = data.routes[0].distance
                //console.log(distance)

                distances.push(distance);

            }
            catch (err) {
                //console.log(err)
            }
            return response
        }
*/

//goes inside onMapClick(e)
/*
distances = [];

            L.popup().setLatLng(e.latlng).setContent(e.latlng.lat + ", " + e.latlng.lng).openOn(map);

            let prepareCoordsForFetch = function (i) {
                let s = '';
                s += origin[0] + ',' + origin[1] + ";";
                s += pointsForRoutingTest[i][0] + ',' + pointsForRoutingTest[i][1];

                return s;
            }

            let origin = [e.latlng.lat, e.latlng.lng];




            let i = -1;
            let isus = async () => {
                pointsForRoutingTest.forEach(element => {
                    i++;
                    //console.log(prepareCoordsForFetch(i))
                    let response = fetchShortestPath(prepareCoordsForFetch(i));

                    response.then(() => {
                        //console.log("Min is " + Math.min(...distances))
                        //console.log("Index of min is " + distances.indexOf(Math.min(...distances)))

                        if (distances.length === pointsForRoutingTest.length) {
                            routing = L.Routing.control({
                                waypoints: [
                                    L.latLng(origin[0], origin[1]),
                                    L.latLng(pointsForRoutingTest[distances.indexOf(Math.min(...distances))][0], pointsForRoutingTest[distances.indexOf(Math.min(...distances))][1])
                                ]
                            }).addTo(map);
                        }
                    })


                });
            }

            isus();
*/