import React from 'react'
import "../styles/navbar.css"
import $ from 'jquery';
import jws from 'jws';

const Navbar = ({ setUserInfoWindow, showUserInfoWindow, setShowCalendarWindow, setShowEditParkingWindow }) => {
    var showInfo = function () {
        console.log('clicked')
        if (jws.decode(localStorage.getItem('token')) && jws.decode(localStorage.getItem('token')).payload.role.includes('PARKING_MANAGER') && $('#popupStatistics').css('display') === 'none') {
            setUserInfoWindow(true);
            if ($('#popupClientInfo').css('display') === 'none')
                $('#button-container-holder').show();
            else
                $('#button-container-holder').hide();
        }
        else if (jws.decode(localStorage.getItem('token')) && jws.decode(localStorage.getItem('token')).payload.role.includes('PARKING_MANAGER')) {
            setUserInfoWindow(true);
            $('#button-container-holder').hide();
        }
        else {
            setUserInfoWindow(!showUserInfoWindow);
            if ($('#popupClientInfo').length === 0)
                $('#button-container-holder').hide();
            else
                $('#button-container-holder').show();
        }
        $('#popupStatistics').hide();
    }

    var popUpManage = function (id1, id2) {
        var popup1 = $(id1);
        var popup2 = $(id2);
        var leafletControlContainer = $('leaflet-control-container');
        //console.log(leafletControlContainer)

        if (popup1.css('display') === 'none' || popup1.css('display') === '') {
            popup1.show();
            popup2.hide();
            $("body").css("overflow", "auto");
            setUserInfoWindow(false);
            $('#button-container-holder').hide();
        } else {
            popup1.hide();
            $("body").css("overflow", "hidden");
            $('#button-container-holder').show();
        }


        if (popup1.css('display') === 'none' && popup2.css('display') === 'none') {
            $('#button-container-holder').show();
        }
    }

    var cancel = function (e) {
        e.preventDefault();
        $('#parking-exists').show();
        setShowEditParkingWindow(false);
    }

    var signout = () => {
        localStorage.removeItem('token');
        window.location.replace("/");
    }

    if (localStorage.getItem('token') === null) {
        return (
            <nav className="navbar">
                <h1><a href="/">ParkShare</a></h1>
                <div className="links">
                    <button onClick={(e) => {
                        popUpManage('#popupLogin', '#popupRegister');
                    }}>Login</button>
                    <button onClick={(e) => {
                        popUpManage('#popupRegister', '#popupLogin');
                    }}>Register</button>
                </div>
            </nav>
        );
    }
    else if (jws.decode(localStorage.getItem('token')).payload.role.includes('CLIENT')) {
        return (
            <nav className="navbar">
                <h1><a href="/home">ParkShare</a></h1>
                <div className="links">
                    <button onClick={() => { showInfo(); setShowCalendarWindow(false); }}>My profile</button>
                    <button onClick={() => signout()}>Signout</button>
                </div>
            </nav>
        );
    }
    else if (jws.decode(localStorage.getItem('token')).payload.role.includes('ADMIN')) {
        return (
            <nav className="navbar">
                <h1><a href="/home">ParkShare</a></h1>
                <div className="links">
                    <button onClick={() => { popUpManage('#popupUserlist', '#popupUnaprovedList'); $('#userListPu').show(); $('#popupEditWindow').hide(); }}>All profiles</button>
                    <button onClick={() => popUpManage('#popupUnaprovedList', '#popupUserlist')}>Unaproved Managers</button>
                    <button onClick={() => signout()}>Signout</button>
                </div>
            </nav>
        );
    } else if (jws.decode(localStorage.getItem('token')).payload.role.includes('PARKING_MANAGER')) {
        return (
            <nav className="navbar">
                <h1><a href="/home">ParkShare</a></h1>
                <div className="links">
                    <button onClick={(e) => { popUpManage('#popupStatistics', '#popupClientInfo'); cancel(e); }}>Statistics</button>
                    <button onClick={(e) => { popUpManage('#popupClientInfo', '#popupStatistics'); showInfo(); cancel(e); }}>My profile</button>
                    <button onClick={() => signout()}>Signout</button>
                </div>
            </nav>
        );
    } else {
        throw Error('Unauthorized access');
    }

}

export default Navbar;