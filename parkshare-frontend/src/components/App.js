import '../styles/App.css';
import Navbar from "./Navbar";
import { useState } from 'react'
import UnauthorizedHome from './UnauthorizedHome';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from './Home';
import Map from './Map';


function App() {
  var domain = 'https://f3rovci-parkshare.herokuapp.com/';


  //POP UP WINDOWS
  const [showCreateParkingWindow, setShowCreateParkingWindow] = useState(false);
  const [showEditParkingSpaceWindow, setShowEditParkingSpaceWindow] = useState(false);
  const [showEditParkingWindow, setShowEditParkingWindow] = useState(false);
  const [showCreateParkingSpaceWindow, setShowCreateParkingSpaceWindow] = useState(false);
  const [showCalendarWindow, setShowCalendarWindow] = useState(false);
  const [showParkingOccupationWindow, setParkingOccupationWindow] = useState(false);
  const [showUserInfoWindow, setUserInfoWindow] = useState(false);

  const polygonData = {
    long1: 0,
    long2: 0,
    long3: 0,
    long4: 0,
    lat1: 0,
    lat2: 0,
    lat3: 0,
    lat4: 0,
  };
  const parkingSpotData = {
    long1: 0,
    long2: 0,
    long3: 0,
    long4: 0,
    lat1: 0,
    lat2: 0,
    lat3: 0,
    lat4: 0,
    parkingLotId: -1
  };

  const [pData, setPolygonData] = useState(polygonData);

  const [psData, setParkingSpaceData] = useState(parkingSpotData);

  const [allParkingSpots, setParkingSpots] = useState([]);

  const [calendarParkingId, setCalendarParkingId] = useState({});

  const [parkingSpot, setParkingSpot] = useState({}); //CLICKED PARKING SPOT

  const [hasParkingLot, setHasParkingLot] = useState(false);

  return (

    <BrowserRouter>
      <div className="App">
        <Navbar setUserInfoWindow={setUserInfoWindow} showUserInfoWindow={showUserInfoWindow} setShowCalendarWindow={setShowCalendarWindow} setShowEditParkingWindow={setShowEditParkingWindow} />
        <Routes>
          <Route exact path="/" element={<UnauthorizedHome domain={domain} />}>
          </Route>
          <Route exact path="/home" element={
            <Home showCreateParkingWindow={showCreateParkingWindow} setShowCreateParkingWindow={setShowCreateParkingWindow} pData={pData} //Creating parking
              showEditParkingSpaceWindow={showEditParkingSpaceWindow} setShowEditParkingSpaceWindow={setShowEditParkingSpaceWindow}
              showEditParkingWindow={showEditParkingWindow} setShowEditParkingWindow={setShowEditParkingWindow} pData={pData}
              showCreateParkingSpaceWindow={showCreateParkingSpaceWindow} setShowCreateParkingSpaceWindow={setShowCreateParkingSpaceWindow} psData={psData}//Creating parking spot
              setParkingSpots={setParkingSpots}
              showCalendarWindow={showCalendarWindow} setShowCalendarWindow={setShowCalendarWindow} calendarParkingId={calendarParkingId} //callendar
              setParkingOccupationWindow={setParkingOccupationWindow}
              showParkingOccupationWindow={showParkingOccupationWindow}
              setParkingSpot={setParkingSpot} parkingSpot={parkingSpot}
              showUserInfoWindow={showUserInfoWindow}
              domain={domain}
            //showEditParkingWindow={showEditParkingWindow} setShowEditParkingWindow={setShowEditParkingWindow} pData={pData}
            />
          }>
          </Route>
        </Routes>
        <Map setShowCreateParkingWindow={setShowCreateParkingWindow} setPolygonData={setPolygonData} //Creating parking
          setShowEditParkingSpaceWindow={setShowEditParkingSpaceWindow}
          setShowEditParkingWindow={setShowEditParkingWindow}
          setShowCreateParkingSpaceWindow={setShowCreateParkingSpaceWindow} setParkingSpaceData={setParkingSpaceData} //Creating parking spot
          allParkingSpots={allParkingSpots}
          setShowCalendarWindow={setShowCalendarWindow} setCalendarParkingId={setCalendarParkingId}
          setParkingOccupationWindow={setParkingOccupationWindow}
          setParkingSpot={setParkingSpot}
          setHasParkingLot={setHasParkingLot}
          domain={domain}
        />
      </div>
    </BrowserRouter>

  );
}

export default App;
