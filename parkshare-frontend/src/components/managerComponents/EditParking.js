import { useEffect, useState } from "react";
import '../../styles/manager.css';
import '../../styles/register.css';
import $ from 'jquery';


const EditParking = ({ setShowEditParkingWindow, pData, domain }) => {

    let [parkingData, setParkingData] = useState({ name: "", description: "", price: "", maxBikes: "" });

    var handleChange = function (event) {
        //load name atribute and value to inputs object
        const name = event.target.name;
        var value = event.target.value;

        setParkingData(values => ({ ...values, [name]: value }));
    }

    useEffect(() => {
        fetchParkingLot(localStorage.getItem('username'));
        //console.log(parkingData)
    }, []);


    var cancel = function (e) {
        e.preventDefault();
        $('#parking-exists').show();
        setShowEditParkingWindow(false);
    }

    function urltoFile(url, filename, mimeType) {
        return (fetch(url)
            .then(function (res) { return res.arrayBuffer(); })
            .then(function (buf) { return new File([buf], filename, { type: mimeType }); })
        );
    }


    /**             CITAJ
     * 
     *              dobijem response 200, ali se nista ne promijeni kada refresham stranicu i ponovno fetcham podatke
     *              
     *              linija 91 baca: SyntaxError: Unexpected token S in JSON at position 0
     * 
     */

    var handleSubmit = async function (event) {
        try {
            //console.log(pData)
            //setShowCreateParkingWindow(false);

            event.preventDefault();

            //const image = document.getElementById("image");
            //parkingData.file = image.files[0];

            console.log(parkingData)

            const image = document.getElementById("image");
            if (image.files[0] !== undefined)
                parkingData.file = image.files[0];
            else
                parkingData.file = await urltoFile(`${parkingData.image}`, 'id.jpg', 'image/jpg');

            const formData = new FormData();

            formData.append('id', parkingData.id);
            formData.append('name', parkingData.name);
            formData.append('description', parkingData.description);
            formData.append('file', parkingData.file);
            formData.append('price1', parkingData.price1);
            formData.append('price5', parkingData.price5);
            formData.append('price10', parkingData.price10);


            //f3rovci-parkshare.herokuapp.com
            const response = await fetch(`${domain}api/manager/edit-parking-lot`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                body: formData
            });

            console.log("res")
            console.log(response);

            if (!response.ok) {
                throw Error('Could not fetch the data');
            }

            console.log(response.status);
            //const data = await response.json();

            //console.log(data);


        } catch (err) {
            console.log(err)
        }
    }

    let fetchParkingLot = async function (username) {
        try {
            const response = await fetch(`${domain}api/manager/show-parking-lots-manager/${username}`, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
            });

            setParkingData(await response.json());

            if (response.ok) {

            }
        } catch (e) {
            console.log(e);
        }
    }


    return (
        <div className="edit-parking popupDark" id='edit-parking'>
            <div className="parking">
                <form className="parkingForm" onSubmit={handleSubmit}>
                    <label className="loginLabel" htmlFor="parkingName" value="Parking name">Parking name:</label>
                    <input className="inputField" type="text" name="name" id="parkingName" onChange={handleChange} value={parkingData.name} />
                    <label className="loginLabel" htmlFor="parkingDescription">Description</label>
                    <textarea className="inputField" id="textarea" type="textarea" name="description" id="parkingDescription" onChange={handleChange} value={parkingData.description} />
                    <input className="inputField" type="file" className="imgField" name="image" id="image" accept="image/*" />
                    <label className="loginLabel" htmlFor="image" className="custom-file-upload">
                        Upload ID image
                    </label>
                    <label className="loginLabel" htmlFor="parkingName" value="Parking name">Parking price per hour:</label>
                    <input className="inputField" type="number" name="price1" onChange={handleChange} step="0.01" value={parkingData.price1} />
                    <label className="loginLabel" htmlFor="parkingName" value="Parking name">Parking price per 5 hours:</label>
                    <input className="inputField" type="number" name="price5" onChange={handleChange} step="0.01" value={parkingData.price5} />
                    <label className="loginLabel" htmlFor="parkingName" value="Parking name">Parking price per 10 hours:</label>
                    <input className="inputField" type="number" name="price10" onChange={handleChange} step="0.01" value={parkingData.price10} />



                    <div className="formCreateParkingButtons">
                        <input className="registerInput" type="submit" id="submit" value="Save" />
                        <button className="cancelButton" id="cancel" onClick={(e) => cancel(e)}>Cancel </button>
                    </div>

                </form>
            </div>
        </div>
    );
}

export default EditParking;