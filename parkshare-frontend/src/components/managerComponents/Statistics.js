import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, ResponsiveContainer } from 'recharts';
import { useEffect, useState } from 'react';
import $ from 'jquery';

const data1 = [
    { name: 'Marko', value: 400 },
    { name: 'Marko', value: 300 },
    { name: 'je', value: 300 },
    { name: 'sitni', value: 200 },
    { name: 'vodozemac', value: 270 },
];

const Statistics = ({ domain }) => {
    let [data, setData] = useState(data1);

    let fetchStatistics = async function () {
        try {
            const formData = new FormData();
            formData.append('username', localStorage.getItem('username'));

            const response = await fetch(`${domain}api/manager/stats`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                body: formData
            });

            if (response.ok) {
                let is = await response.json()

                for (let i = 0; i < is.length; i++) {
                    var date = new Date();
                    var yesterday = date - 1000 * 60 * 60 * 24 * Math.abs(i - is.length);
                    yesterday = new Date(yesterday);

                    setData(oldData => ([
                        ...oldData, {
                            name: yesterday.getDate() + "." + (parseInt(yesterday.getMonth()) + 1) + ".",
                            value: is[i]
                        }]))
                }
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    useEffect(() => {
        setData([]);
        fetchStatistics();
    }, []);

    return (
        <div className="popupDarkList" id="popupStatistics">
            <div className="statistics" id="statistics">
                <h1>Statistika za parkiralište</h1>

                <br />
                <LineChart width={600} height={300} data={data} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
                    <Line type="monotone" dataKey="value" stroke="#8884d8" />
                    <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                </LineChart>
            </div>
        </div>

    );
}

export default Statistics;