import { useEffect, useState } from "react";
import '../../styles/manager.css';
import '../../styles/register.css';
import $ from 'jquery';

const CreateParking = ({ setShowEditParkingSpaceWindow, setParkingSpot, parkingSpot, domain }) => {

    const [inputs, setInputs] = useState({ bookable: "false" });

    useEffect(() => {
        console.log(parkingSpot)
        setInputs({ bookable: parkingSpot.bookable });
    }, [parkingSpot]);

    var handleChange = function (event) {
        //load name atribute and value to inputs object
        const name = event.target.name;
        var value = event.target.value;

        setInputs(values => ({ ...values, [name]: value }));
    }

    var cancel = function (e) {
        e.preventDefault();
        $('#parking-exists').show();
        setShowEditParkingSpaceWindow(false);
    }

    var deleteParkingSpace = async function (e) {
        try {
            e.preventDefault();
            const formData = new FormData();
            formData.append('id', parkingSpot.parkingSpaceId);

            const response = await fetch(`${domain}api/manager/ps-delete`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                body: formData
            });

            if (response.ok)
                window.location.reload();
        } catch (e) {
            console.log(e);
        }
        setShowEditParkingSpaceWindow(false);
    }

    var handleSubmit = async function (event) {
        event.preventDefault();

        //ISUS
        const formData = new FormData();
        formData.append('id', parkingSpot.parkingSpaceId);
        formData.append('bookable', inputs.bookable);

        // isBookable = inputs.bookable === "true";

        /*for (var value of formData.values()) {
            console.log(value);
        }*/

        try {
            const response = await fetch(`${domain}api/manager/ps-reservable`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                body: formData
            });

            let ps = parkingSpot;
            ps.bookable = inputs.bookable;

            setParkingSpot(ps);

            //console.log(response);

            $('#parking-exists').show();

            setShowEditParkingSpaceWindow(false);
        }
        catch (e) {
            console.log(e);
        }
    }

    return (
        <div className="edit-parking-space popupDark" id='edit-parking-space'>
            <div className="parking-space">
                <form className="parkingForm" onSubmit={handleSubmit}>
                    <label className="reserve" htmlFor="bookable">Allow reserve</label>
                    <select name="bookable" id="bookable" onChange={handleChange} value={inputs.bookable}>
                        <option value="true">Yes</option>
                        <option value="false">No</option>
                    </select>
                    <div className="formCreateParkingButtons">
                        <input className="registerInput" type="submit" id="submit" value="Save" />
                        <button className="cancelButton" id="cancel" onClick={(e) => cancel(e)}>Cancel </button>
                        <button className="deleteButton" id="delete" onClick={(e) => deleteParkingSpace(e)}>Delete </button>
                    </div>

                </form>
            </div>
        </div >
    );
}

export default CreateParking;