import { useEffect, useState } from "react";
import '../../styles/manager.css';
import '../../styles/register.css';
import $ from 'jquery';

const CreateParking = ({ setShowCreateParkingWindow, pData, domain }) => {


    //ADD hourPrice: , fiveHourPrice: , tenHourPrice: 
    const [inputs, setInputs] = useState({ name: "", description: "", file: "", price1: "", price5: "", price10: "", maxBikes: "" })

    useEffect(() => {
        //console.log(pData);

    }, []);

    $(document).on("keydown", function (e) {
        if (e.keyCode === 27) {
            //setShowCreateParkingWindow(false);
        }
    });

    var handleChange = function (event) {
        //load name atribute and value to inputs object
        const name = event.target.name;
        var value = event.target.value;

        setInputs(values => ({ ...values, [name]: value }));
    }

    $(document).on("keydown", function (e) {
        if (e.keyCode === 27) {
            $('#drawButton').show();
            $('#publishButton').show();
            setShowCreateParkingWindow(false);
        }
    });

    var cancel = function (e) {
        e.preventDefault();
        $('#drawButton').show();
        $('#publishButton').show();
        setShowCreateParkingWindow(false);
    }

    var handleSubmit = async function (event) {
        try {
            //console.log(pData)
            //setShowCreateParkingWindow(false);

            event.preventDefault();

            const image = document.getElementById("image");
            inputs.file = image.files[0];

            const formData = new FormData();

            formData.append('longitude1', pData.long1);
            formData.append('longitude2', pData.long2);
            formData.append('longitude3', pData.long3);
            formData.append('longitude4', pData.long4);
            formData.append('latitude1', pData.lat1);
            formData.append('latitude2', pData.lat2);
            formData.append('latitude3', pData.lat3);
            formData.append('latitude4', pData.lat4);

            // formData.append('longitudeCenter', pData.longitudeCenter);
            // formData.append('latitudeCenter', pData.latitudeCenter);
            formData.append('username', localStorage.getItem('username'));

            for (var key in inputs) {
                formData.append(key, inputs[key]);
            }


            for (var value of formData.values()) {
                console.log(value);
            }

            //f3rovci-parkshare.herokuapp.com
            const response = await fetch(`${domain}api/manager/create-parking-lot`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                body: formData
            });

            console.log("res")
            console.log(response);

            if (!response.ok) {
                throw Error('Could not fetch the data');
            }

            console.log(response.status);
            const data = await response.json();

            console.log(data);

            for (var value of formData.values()) {
                console.log(value);
            }
            setShowCreateParkingWindow(false);

            window.location.reload();

        } catch (err) {
            console.log(err)
        }
    }

    return (
        <div className="create-parking popupDark" id="create-parking">
            <div className="parking">
                <form className="parkingForm" onSubmit={handleSubmit}>
                    <input className="inputField" type="text" name="name" onChange={handleChange} placeholder=" Parking name.." />
                    <textarea className="inputField" id="textarea" type="textarea" name="description" onChange={handleChange} placeholder=" Description.." />
                    <input className="inputField" type="file" className="imgField" name="image" id="image" accept="image/*" />
                    <label className="registerLabel" htmlFor="image" className="custom-file-upload">
                        Upload ID image
                    </label>
                    <input className="inputField" type="number" name="price1" onChange={handleChange} placeholder=" Parking price per hour.." step="0.01" />
                    <input className="inputField" type="number" name="price5" onChange={handleChange} placeholder=" Parking price per 5 hours.." step="0.01" />
                    <input className="inputField" type="number" name="price10" onChange={handleChange} placeholder=" Parking price per 10 hours.." step="0.01" />
                    <input className="inputField" type="number" name="maxBikes" onChange={handleChange} placeholder=" Number of bike spaces.." />

                    <div className="formCreateParkingButtons">
                        <input className="registerInput" type="submit" id="submit" value="Register" />
                        <button className="cancelButton" id="cancel" onClick={(e) => cancel(e)}>Cancel </button>
                    </div>

                </form>
            </div>
        </div>
    );
}

export default CreateParking;