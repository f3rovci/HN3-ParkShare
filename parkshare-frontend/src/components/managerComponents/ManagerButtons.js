import $ from 'jquery';
import React from 'react';
import { useEffect, useRef } from 'react';
import L from 'leaflet';
import { useMap } from 'react-leaflet';

require('leaflet-path-transform');
var handler = require('leaflet-path-drag');
const ManagerButtons = () => {

    let isDrawingPolygon = false;
    let isCreatingParkingSpace = false;

    let polygon;
    let parkingSpaces = [];

    let map = useMap();
    console.log(map)
    map.on('click', onMapClick);


    var offs = [0.00002057048, 0.00002202914];
    function onMapClick(e) {
        //console.log(e);

        if (isDrawingPolygon) {
            if (polygon === undefined) {
                polygon = L.polygon([
                    [e.latlng.lat, e.latlng.lng]
                ]).addTo(map);
            }
            else {
                let points = polygon._latlngs[0];
                points.push(e.latlng);
                polygon.remove();

                polygon = L.polygon(points).addTo(map).on('click', () => { polygon.remove(); polygon = undefined; });
            }

            console.log(polygon._parts)
        }
        if (isCreatingParkingSpace) {
            //  because parking space is small will throw error if you try to make one while zoomed out really far
            var pts = [
                [e.latlng.lat + offs[0], e.latlng.lng + offs[1]],
                [e.latlng.lat + offs[0], e.latlng.lng - offs[1]],
                [e.latlng.lat - offs[0], e.latlng.lng - offs[1]],
                [e.latlng.lat - offs[0], e.latlng.lng + offs[1]],
            ];

            var parking = L.rectangle(pts, { transform: true, draggable: true }).addTo(map);
            parking.transform.disable();
            parking.dragging.disable();
            parkingSpaces.push(parking);
            map.fitBounds(pts);
        }
    }

    var drawPolygonFn = () => {
        isDrawingPolygon = !isDrawingPolygon;

        if (isDrawingPolygon === true) {
            document.getElementById('createParking').classList.add('buttonActive');
        }
        else {
            document.getElementById('createParking').classList.remove('buttonActive');
        };
    }

    var addPSFn = () => {
        isCreatingParkingSpace = !isCreatingParkingSpace;

        if (isCreatingParkingSpace === true) {
            document.getElementById('addParkingSpace').classList.add('buttonActive');
            document.getElementById('editParkingSpace').classList.remove('buttonActive');

            parkingSpaces.forEach(element => {
                element.transform.disable();
                element.dragging.disable();
            });
        }
        else {
            document.getElementById('addParkingSpace').classList.remove('buttonActive');
        };
    }

    var editPSFn = () => {
        if (document.getElementById('editParkingSpace').classList.contains('buttonActive')) {
            parkingSpaces.forEach(element => {
                element.transform.disable();
                element.dragging.disable();
            });
            document.getElementById('editParkingSpace').classList.remove('buttonActive');
        }
        else {
            parkingSpaces.forEach(element => {
                element.transform.setOptions({ scaling: true, rotation: true }).enable();
                element.dragging.enable();
                console.log(element);
            });

            isCreatingParkingSpace = false;
            document.getElementById('editParkingSpace').classList.add('buttonActive');
            document.getElementById('addParkingSpace').classList.remove('buttonActive');
        }
    }


    const ref = useRef()
    useEffect(() => {
        if (ref?.current) {
            const disableClickPropagation = L?.DomEvent?.disableClickPropagation
            disableClickPropagation(ref.current)
        }
    }, [])

    return (
        <div className="manager-buttons" id="buttons" ref={ref}>
            <button className="btn" id='createParking' onClick={drawPolygonFn}>Create parking</button>
            <button className="btn" id='addParkingSpace' onClick={addPSFn}>Add parking space</button>
            <button className="btn" id='editParkingSpace' onClick={editPSFn}>Edit parking spaces</button>
        </div>
    );


}

export default ManagerButtons;