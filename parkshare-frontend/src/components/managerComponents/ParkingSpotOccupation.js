import { useEffect, useState } from "react";
import '../../styles/manager.css';
import '../../styles/register.css';
import $ from 'jquery';
import Select from 'react-select';

const ParkingSpotOccupation = ({ setParkingOccupationWindow, setParkingSpot, parkingSpot, domain }) => {

    const [inputs, setInputs] = useState({ occupy: "false" });

    var isBookable;

    useEffect(() => {
        //console.log(parkingSpot)
        setInputs({ occupy: parkingSpot.available === undefined ? 'false' : !parkingSpot.available });
    }, []);

    $(document).on("keydown", function (e) {
        if (e.keyCode === 27) {
            setParkingOccupationWindow(false);
        }
    });

    var handleChange = function (event) {
        //load name atribute and value to inputs object
        const name = event.target.name;
        var value = event.target.value;
        //console.log(value);
        setInputs(values => ({ ...values, occupy: value }));
    }

    var cancel = function (e) {
        e.preventDefault();
        $('#parking-exists').show();
        setParkingOccupationWindow(false);
    }

    var handleSubmit = async function (event) {
        event.preventDefault();

        //ISUS
        const formData = new FormData();
        formData.append('parkingSpaceId', parkingSpot.parkingSpaceId);
        formData.append('occupy', inputs.occupy);

        // isBookable = inputs.bookable === "true";

        /*for (var value of formData.values()) {
            console.log(value);
        }*/

        try {
            const response = await fetch(`${domain}api/manager/manage-parking-space?parkingSpaceId=${parkingSpot.parkingSpaceId}&occupy=${inputs.occupy}`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
            });

            //console.log(response);
            var info = await response.text();
            console.log(info)
            $('#parking-exists').show();

            if (response.ok) {
                let ps = parkingSpot;
                ps.available = inputs.occupy;

                setParkingSpot(ps);
            }


            setParkingOccupationWindow(false);
            window.location.reload();
        }
        catch (e) {
            console.log(e);
        }
    }


    return (
        <div className="parking-space-occupation popupDark" id='parking-space-occupation'>
            <div className="parking-space">
                <form className="parkingForm" onSubmit={handleSubmit}>
                    <label className="reserve" htmlFor="available">Set occupied</label>

                    <select name="available" id="available" onChange={handleChange} value={inputs.occupy}>
                        <option value="true">Yes</option>
                        <option value="false">No</option>
                    </select>
                    <div className="formCreateParkingButtons">
                        <input className="registerInput" type="submit" id="submit" value="Save" />
                        <button className="cancelButton" id="cancel" onClick={(e) => cancel(e)}>Cancel </button>
                    </div>

                </form>
            </div>
        </div >
    );
}

export default ParkingSpotOccupation;