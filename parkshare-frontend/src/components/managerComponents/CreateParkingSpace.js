import { useEffect, useState } from "react";
import '../../styles/manager.css';
import '../../styles/register.css';
import $ from 'jquery';

const CreateParkingSpace = ({ setShowCreateParkingSpaceWindow, psData, setParkingSpots, domain }) => {

    const [inputs, setInputs] = useState({ label: "" });

    useEffect(() => {
        console.log(psData);
    }, []);

    var handleChange = function (event) {
        //load name atribute and value to inputs object
        const name = event.target.name;
        var value = event.target.value;

        setInputs(values => ({ ...values, [name]: value }));
    }

    var cancel = function (e) {
        e.preventDefault();
        /*$('#drawButton').show();
        $('#publishButton').show();*/
        setShowCreateParkingSpaceWindow(false);
        window.location.reload();
    }

    var handleSubmit = async function (event) {
        try {
            //console.log(pData)
            //setShowCreateParkingWindow(false);

            event.preventDefault();

            console.log(inputs)
            const formData = new FormData();

            formData.append('longitude1', psData.long1);
            formData.append('longitude2', psData.long2);
            formData.append('longitude3', psData.long3);
            formData.append('longitude4', psData.long4);
            formData.append('latitude1', psData.lat1);
            formData.append('latitude2', psData.lat2);
            formData.append('latitude3', psData.lat3);
            formData.append('latitude4', psData.lat4);
            formData.append('parkingLotId', psData.parkingLotId);

            const parkingSpace = {
                long1: psData.long1,
                long2: psData.long2,
                long3: psData.long3,
                long4: psData.long4,
                lat1: psData.lat1,
                lat2: psData.lat2,
                lat3: psData.lat3,
                lat4: psData.lat4,
                parkingLotId: psData.parkingLotId,
                parkingSpaceId: -1,
                label: inputs.label,
                booking: ""
            };

            for (var key in inputs) {
                formData.append(key, inputs[key]);
            }


            // for (var value of formData.values()) {
            //     console.log(value);
            // }

            //f3rovci-parkshare.herokuapp.com
            const response = await fetch(`${domain}api/manager/create-parking-space`, {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
                body: formData
            });

            console.log("res")
            console.log(response);

            if (!response.ok) {
                throw Error('Could not fetch the data');
            }

            console.log(response.status);
            const data = await response.json();

            console.log(data);

            //parkingSpace.parkingSpaceId = data.parkingSpaceId;

            console.log(parkingSpace)

            //setParkingSpots(oldSpots => [...oldSpots, parkingSpace]);

            setShowCreateParkingSpaceWindow(false);

        } catch (err) {
            console.log(err)
        }
    }

    return (
        <div className="create-parking popupDark" id="create-parking">
            <div className="parking">
                <form className="parkingForm" onSubmit={handleSubmit}>
                    <input className="inputField" type="text" name="label" onChange={handleChange} placeholder=" Parking label.." />

                    <div className="formCreateParkingButtons">
                        <input className="registerInput" type="submit" id="submit" value="Register" />
                        <button className="cancelButton" id="cancel" onClick={e => cancel(e)}>Cancel </button>
                    </div>

                </form>
            </div >
        </div >
    );
}

export default CreateParkingSpace;