import jws from 'jws';
import '../../styles/userStyles/clientInfo.css'
import { useState, useEffect } from 'react';

const ClientInfo = ({ domain }) => {
    const defaultInfo = {
        username: '',
        email: '',
        firstname: '',
        lastname: '',
        role: '',
        iban: '',
        walletBalance: ''
    };
    const [info, setInfo] = useState(defaultInfo);

    if (localStorage.getItem('token')) {
        var username = jws.decode(localStorage.getItem('token')).payload.sub;
        var role = (jws.decode(localStorage.getItem('token')).payload.role.includes('CLIENT')) ? 'client' : 'manager';
    }

    useEffect(() => {
        console.log('Fething ... ')
        fetchF();
    }, []);

    const fetchF = async () => {
        try {
            //client || manager
            let response = await fetch(`${domain}api/${role}/user/${username}`, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                }
            });

            if (!response.ok) {
                localStorage.removeItem('token');
                window.location.replace('/');
            }

            let jason = await response.json();
            //console.log(jason);

            setInfo(values => ({
                ...values,
                username: jason.username,
                email: jason.email,
                firstname: jason.firstName,
                lastname: jason.lastName,
                role: jason.role,
                iban: jason.iban,
                walletBalance: jason.walletBalance,
                image: jason.image
            }));

        } catch (err) {

        }

    };

    var addBalance = async function () {
        const input = document.getElementById('input1');
        const value = input.value;
        console.log(input.className);

        if (input.className === "hideInput") {
            input.className = "showInput";
        } else {
            try {
                const formData = new FormData();
                formData.append('amount', value);
                formData.append('username', username);

                let response = await fetch(`${domain}api/client/money-transfer`, {
                    method: 'POST',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('token')
                    },
                    body: formData
                });

                let data = await response.json();

                if (data.successful === "false") {
                    alert(data.message);
                }

                fetchF();
                input.className = "hideInput";

            } catch (err) {

            }
        }
    }


    //add image
    if (info.role === "CLIENT") {
        return (
            <div id="popupClientInfo" className="popupDarkList">
                <div className="clientInfoWindow">
                    <h2 className="clientInfoText">Your information</h2>
                    <hr />
                    <h3 className="clientInfoInformation">Username: {info.username}</h3>
                    <h3 className="clientInfoInformation">Role: {info.role}</h3>
                    <h3 className="clientInfoInformation">Email: {info.email}</h3>
                    <h3 className="clientInfoInformation">Name: {info.firstname}</h3>
                    <h3 className="clientInfoInformation">Last name: {info.lastname}</h3>
                    <h3 className="clientInfoInformation">IBAN: {info.iban}</h3>
                    <h3 className="clientInfoInformation">Balance: {info.walletBalance}</h3>
                    <input type="number" className="hideInput" id="input1" /> <br />
                    <button id="button" type="button" onClick={addBalance}>Add balance</button>
                    <h3 className="clientInfoInformation">ID image:</h3>
                    <div className="clientInfoImage">
                        <img id="imageID" src={info.image} height="300wh" />
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div id="popupClientInfo" className="popupDarkList">
                <div className="clientInfoWindow">
                    <h2 className="clientInfoText">Your information</h2>
                    <hr />
                    <h3 className="clientInfoInformation">Username: {info.username}</h3>
                    <h3 className="clientInfoInformation">Role: {info.role}</h3>
                    <h3 className="clientInfoInformation">Email: {info.email}</h3>
                    <h3 className="clientInfoInformation">Name: {info.firstname}</h3>
                    <h3 className="clientInfoInformation">Last name: {info.lastname}</h3>
                    <h3 className="clientInfoInformation">IBAN: {info.iban}</h3>
                    <h3 className="clientInfoInformation">ID image:</h3>
                    <div className="clientInfoImage">
                        <img src={info.image} height="300wh" />
                    </div>
                </div>
            </div>
        );
    }
}

export default ClientInfo;